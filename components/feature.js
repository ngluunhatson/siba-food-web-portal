import Hero from "./hero";
import Search from "./search";
import _Carousel from "./carouel";
import Gallery from "./gallery";
import List from "./list";
import Research from "./research";
import Form from "./form";
import FormPrivate from './formPrivate';
import Subscribe from './subscribe';
import Switcher from "@/components/switcher";
import FabButton from "@/components/fabButton";

export default function Feature(props) {
  let eleHTML;
  switch (props.component) {
    case "hero":
      eleHTML = (
        <Hero
          url={ props.url }
          display={ props.display }
          options={ props.options }
          primaryColor={ props.primaryColor }
          secondaryColor={ props.secondaryColor }
          backgroundPrimaryBased={ props.backgroundPrimaryBased }
          backgroundLightGrey={ props.backgroundLightGrey }
          image={ props.image }
          windowDimension={ props.windowDimension }
        />
      );
      break;
    case "search":
      eleHTML = <Search url={ props.url } display={ props.display } windowDimension={ props.windowDimension }
                        options={ props.options }/>;
      break;
    case "carousel":
      eleHTML = (
        <_Carousel
          image={ props.image }
          url={ props.url }
          options={ props.options }
          display={ props.display }
          background={ props.background }
          windowDimension={ props.windowDimension }
        />
      );
      break;
    case "gallery":
      eleHTML = (
        <Gallery
          image={ props.image }
          url={ props.url }
          display={ props.display }
          options={ props.options }
          background={ props.background }
          windowDimension={ props.windowDimension }
        />
      );
      break;
    case "list":
      eleHTML = (
        <List image={ props.image } url={ props.url } display={ props.display } options={ props.options }
              primaryColor={ props.primaryColor } secondaryColor={ props.secondaryColor }
              windowDimension={ props.windowDimension }/>
      );
      break;
    case "research":
      eleHTML = (
        <Research
          image={ props.image }
          url={ props.url }
          display={ props.display }
          options={ props.options }
          background={ props.background }
        />
      );
      break;
    case "form":
      eleHTML = (
        <Form display={ props.display } url={ props.url } options={ props.options }/>
      );
      break;
    case "form-private":
      eleHTML = <FormPrivate display={ props.display } url={ props.url } options={ props.options }/>;
      break;
    case "subscribe":
      eleHTML = <Subscribe display={ props.display } options={ props.options } background={ props.background }
                           windowDimension={ props.windowDimension }/>;
      break;

    case "switcher":
      eleHTML =
        <Switcher display={ props.display } options={ props.options } windowDimension={ props.windowDimension }/>;
      break;

    case "fab-button":
      eleHTML = <FabButton display={ props.display } options={ props.options }/>;
      break;
    default:
      break;
  }
  return eleHTML || <></>;
}
