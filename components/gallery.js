/* eslint-disable react-hooks/exhaustive-deps */
import { getDataFromCollection } from "@/services/services";
import { Box, Card, Center, Divider, Flex, Grid, Group, Image, Loader, SimpleGrid, Stack, Text, } from "@mantine/core";
import React, { useEffect, useState } from "react";
import { ChevronRight } from "tabler-icons-react";
import styles from "/styles/feature.module.scss";
import variables from "/styles/variables.module.scss";
import Link from "next/link";
import globals from "/styles/globals.module.scss";


export default function Gallery(props) {
  let eleHTML = <></>;
  switch (props.display) {
    case "gallery1":
      eleHTML = <Gallery1 url={ props.url }
                          title={ props.options.title }
                          logo={ props.image }
                          optionData={ props.options.data }
                          optionQuery={ '' }
                          optionJsonata={ props.options.jsonata }
                          optionTypeChild={ props.options.child }
                          background={ props.background }/>
      break;

    //Galery cho News
    case "gallery2":
      if (props.windowDimension.width >= 720)
        eleHTML = <Gallery2 url={ props.url }
                            title={ props.options.title }
                            logo={ props.image }
                            optionData={ props.options.data }
                            optionQuery={ '' }
                            optionJsonata={ props.options.jsonata }
                            optionTypeChild={ props.options.child }
                            background={ props.background }
                            isFirstImageSlide={ false }/>

      break;

    // Gallery thể hiện hình ảnh trong /Introduction
    case "gallery3":

      eleHTML = <Gallery3R { ...props.options } windowDimension={ props.windowDimension }/>

      break;

    //Gallery cho /news kiểu 1 vertical card bên trái và 2 horizontal card bên phải
    case "gallery4":
      eleHTML = <Gallery4 url={ props.url }
                          title={ props.options.title }
                          logo={ props.image }
                          optionData={ props.options.data }
                          optionQuery={ '' }
                          optionJsonata={ props.options.jsonata }
                          optionTypeChild={ props.options.child }/>

      break;

      break;
    default:
      break;
  }
  return eleHTML;
}

function loadData(loadOptions, setDataFunc, sortByDate = false) {
  const loadDataFunc = async () => await getDataFromCollection(
    loadOptions.url, loadOptions.data, loadOptions.query, loadOptions.jsonata
  );
  loadDataFunc().then(res => {
    if (sortByDate) {
      const resSorted = res.sort((a, b) => {
        const date1 = new Date(a.date);
        const date2 = new Date(b.date);
        return date2 - date1;
      });
      setDataFunc(resSorted);
    } else {
      setDataFunc(res);
    }
  });
}

function Gallery1(props) {
  const [data, setData] = useState([]);
  useEffect(() => {
    loadData({
      url: props.url,
      data: props.optionData,
      query: props.optionQuery,
      jsonata: props.optionJsonata
    }, setData, true);
  }, []);
  const eleHTML = (
    <Box
      className={ styles.gallery1 }
      sx={ (theme) => ({
        padding: "0px 64px",
      }) }
    >
      <div
        className={ styles.container }
        style={ {background: `${ props.background }66`} }
      >
        <div className={ styles.spaceBetween }>
          <Stack>
            <Text fz={ 48 } fw={ 700 } c={ variables.primaryColor }>
              { props.title }
            </Text>
            <div className={ styles.divider }>
              <Divider size={ 1 } color={ variables.primaryColor }/>
            </div>
          </Stack>
          <Group>
            <Text fz={ 20 } c={ variables.primaryColor }>
              Xem tất cả
            </Text>
            <ChevronRight
              size={ 24 }
              color={ variables.primaryColor }
              strokeWidth={ 2 }
            />
          </Group>
        </div>
        <Grid mt={ 32 }>
          <Grid.Col span={ 6 } pr={ 16 }>
            <Card radius={ 40 } h={ 800 }>
              <Card.Section>
                <Image
                  src={
                    data[0] != undefined
                      ? data[0]?.image
                      : "/images/no-img.jpeg"
                  }
                  placeholder={ <Image src={ props.logo } alt=""></Image> }
                  withPlaceholder
                  height={ 400 }
                  alt={ data[0] != undefined ? data[0]?.name : "" }
                />
              </Card.Section>
              <Box className={ styles.cardBody }>
                <Link
                  href={ `${ props.optionData }/${
                    data[0] != undefined ? data[0]?.id : "id-0"
                  }?type_child=${ props.optionTypeChild }` }
                >
                  <Text className={ styles.name } lineClamp={ 2 }>
                    { data[0] != undefined ? data[0]?.name : "" }
                  </Text>
                </Link>
                <Text lineClamp={ 3 }>
                  { data[0] != undefined ? data[0]?.excerpt : "" }
                </Text>
                <Link
                  href={ `${ props.optionData }/${
                    data[0] != undefined ? data[0]?.id : "id-0"
                  }?type_child=${ props.optionTypeChild }` }
                >
                  <Group className={ styles.positionBottom }>
                    <Text fz={ 20 } c={ variables.primaryColor }>
                      Xem tất cả
                    </Text>
                    <ChevronRight
                      size={ 24 }
                      color={ variables.primaryColor }
                      strokeWidth={ 2 }
                    />
                  </Group>
                </Link>
              </Box>
            </Card>
          </Grid.Col>
          <Grid.Col span={ 6 } pl={ 16 }>
            <SimpleGrid cols={ 2 } spacing={ 32 }>
              <div className={ styles.item }>
                <Image
                  src={
                    data[1] != undefined
                      ? data[1]?.image
                      : "/images/no-img.jpeg"
                  }
                  alt={ data[1] != undefined ? data[1]?.name : "" }
                  placeholder={ <Image src={ props.logo } alt=""></Image> }
                  withPlaceholder
                  height={ 384 }
                />
                <Box
                  className={ `${ styles.cardBody } ${ styles.itemBody } ${ styles.slide }` }
                >
                  <Link
                    href={ `${ props.optionData }/${
                      data[1] != undefined ? data[1]?.id : "id-1"
                    }?type_child=${ props.optionTypeChild }` }
                  >
                    <Text className={ styles.name } lineClamp={ 2 }>
                      { data[1] != undefined ? data[1]?.name : "" }
                    </Text>
                  </Link>
                  <Text lineClamp={ 4 }>
                    { data[1] != undefined ? data[1]?.excerpt : "" }
                  </Text>
                  <Link
                    href={ `${ props.optionData }/${
                      data[0] != undefined ? data[1]?.id : "id-0"
                    }?type_child=${ props.optionTypeChild }` }
                  >
                    <Group className={ styles.positionBottom }>
                      <Text fz={ 20 } c={ variables.primaryColor }>
                        Xem tất cả
                      </Text>
                      <ChevronRight
                        size={ 24 }
                        color={ variables.primaryColor }
                        strokeWidth={ 2 }
                      />
                    </Group>
                  </Link>
                </Box>
              </div>
              <div className={ styles.item }>
                <Box className={ `${ styles.cardBody } ${ styles.itemBody }` }>
                  <Link
                    href={ `${ props.optionData }/${
                      data[2] != undefined ? data[2]?.id : "id-2"
                    }?type_child=${ props.optionTypeChild }` }
                  >
                    <Text className={ styles.name } lineClamp={ 2 }>
                      { data[2] != undefined ? data[2]?.name : "" }
                    </Text>
                  </Link>
                  <Text lineClamp={ 4 }>
                    { data[2] != undefined ? data[2]?.excerpt : "" }
                  </Text>
                  <Group className={ styles.positionBottom }>
                    <Text fz={ 20 } c={ variables.primaryColor }>
                      Xem tất cả
                    </Text>
                    <ChevronRight
                      size={ 24 }
                      color={ variables.primaryColor }
                      strokeWidth={ 2 }
                    />
                  </Group>
                </Box>
                <Link
                  href={ `${ props.optionData }/${
                    data[2] != undefined ? data[0]?.id : "id-0"
                  }?type_child=${ props.optionTypeChild }` }
                >
                  <Image
                    src={
                      data[2] != undefined
                        ? data[2]?.image
                        : "/images/no-img.jpeg"
                    }
                    alt={ data[2] != undefined ? data[2]?.name : "" }
                    placeholder={ <Image src={ props.logo } alt=""></Image> }
                    withPlaceholder
                    height={ 384 }
                    className={ styles.slide }
                  />
                </Link>
              </div>
              <div className={ styles.item }>
                <Box className={ `${ styles.cardBody } ${ styles.itemBody }` }>
                  <Link
                    href={ `${ props.optionData }/${
                      data[3] != undefined ? data[3]?.id : "id-3"
                    }?type_child=${ props.optionTypeChild }` }
                  >
                    <Text className={ styles.name } lineClamp={ 2 }>
                      { data[3] != undefined ? data[3]?.name : "" }
                    </Text>
                  </Link>
                  <Text lineClamp={ 4 }>
                    { data[3] != undefined ? data[3]?.excerpt : "" }
                  </Text>
                  <Group className={ styles.positionBottom }>
                    <Text fz={ 20 } c={ variables.primaryColor }>
                      Xem tất cả
                    </Text>
                    <ChevronRight
                      size={ 24 }
                      color={ variables.primaryColor }
                      strokeWidth={ 2 }
                    />
                  </Group>
                </Box>
                <Link
                  href={ `${ props.optionData }/${
                    data[3] != undefined ? data[3]?.id : "id-3"
                  }?type_child=${ props.optionTypeChild }` }
                >
                  <Image
                    src={
                      data[3] != undefined
                        ? data[3]?.image
                        : "/images/no-img.jpeg"
                    }
                    alt={ data[3] != undefined ? data[3]?.name : "" }
                    placeholder={ <Image src={ props.logo } alt=""></Image> }
                    withPlaceholder
                    height={ 384 }
                    className={ styles.slide }
                  />
                </Link>
              </div>
              <div className={ styles.item }>
                <Image
                  src={
                    data[4] != undefined
                      ? data[4]?.image
                      : "/images/no-img.jpeg"
                  }
                  alt={ data[4] != undefined ? data[4]?.name : "" }
                  placeholder={ <Image src={ props.logo } alt=""></Image> }
                  withPlaceholder
                  height={ 384 }
                />
                <Box
                  className={ `${ styles.cardBody } ${ styles.itemBody } ${ styles.slide }` }
                >
                  <Link
                    href={ `${ props.optionData }/${
                      data[4] != undefined ? data[4]?.id : "id-4"
                    }?type_child=${ props.optionTypeChild }` }
                  >
                    <Text className={ styles.name } lineClamp={ 2 }>
                      { data[4] != undefined ? data[4]?.name : "" }
                    </Text>
                  </Link>
                  <Text lineClamp={ 4 }>
                    { data[4] != undefined ? data[4]?.excerpt : "" }
                  </Text>
                  <Link
                    href={ `${ props.optionData }/${
                      data[4] != undefined ? data[3]?.id : "id-3"
                    }?type_child=${ props.optionTypeChild }` }
                  >
                    <Group className={ styles.positionBottom }>
                      <Text fz={ 20 } c={ variables.primaryColor }>
                        xem chi tiết
                      </Text>
                      <ChevronRight
                        size={ 24 }
                        color={ variables.primaryColor }
                        strokeWidth={ 2 }
                      />
                    </Group>
                  </Link>
                </Box>
              </div>
            </SimpleGrid>
          </Grid.Col>
        </Grid>
      </div>
    </Box>
  );
  return eleHTML;
}

function Gallery2(props) {
  const [data, setData] = useState([]);
  useEffect(() => {
    loadData({
      url: props.url,
      data: props.optionData,
      query: props.optionQuery,
      jsonata: props.optionJsonata
    }, setData, true);
  }, []);

  const slideCardJSX = [];

  if (data.length > 1) {
    let stackCardJSX = [];
    let isImageSlideCard = props.isFirstImageSlide;
    const originalIsImageSlideCard = isImageSlideCard;

    for (let i = 1; (i < data.length && i < 5); i++) {
      stackCardJSX.push(
        <Gallery2SlideComponent
          key={ `slide-card-${ i }` }
          imageComponentProps={ {
            image: data[i].image,
            alt: '',
            placeholder: <Loader color={ variables.primaryColor } variant="bars"/>,
            width: 320,
            height: 320
          } }
          textComponentProps={ {
            text: data[i].name,
          } }
          link={ `${ props.optionData }/${ data[i].id }?type_child=${ props.optionTypeChild }` }
          isImageSlideCard={ isImageSlideCard }/>
      );
      isImageSlideCard = !isImageSlideCard;
      if (i % 2 === 0 || i + 1 === data.length || i + 1 === 5) {
        isImageSlideCard = !originalIsImageSlideCard;
        const returnStackCardJSX = stackCardJSX.splice(0);
        stackCardJSX = [];
        slideCardJSX.push(
          <Stack spacing={ 32 } key={ `stack-slide-${ slideCardJSX.length }` }>
            { returnStackCardJSX }
          </Stack>
        );

      }
    }
  }

  const eleHTML = (
    <>
      <Box className={ styles.gallery2 }>
        <Stack spacing={ 32 } className={ styles.gallery2Main }>
          <Stack spacing={ 16 }>
            <Text className={ globals.heading1 }>{ props.title }</Text>
            <Divider size={ 1 } color={ variables.secondColor } style={ {width: "100px"} }/>
          </Stack>
          <Group spacing={ 32 } className={ styles.gallery2Body }>
            <Link className={ styles.firstCard }
                  href={ `${ props.optionData }/${
                    data.length > 0 ? data[0].id : "id-0"
                  }?type_child=${ props.optionTypeChild }` }>
              <Stack spacing={ 0 }>
                <Image radius={ "40px 40px 0px 0px" }
                       width={ 520 }
                       height={ 385.24 }
                       src={ data.length > 0 ? data[0].image : props.logo }
                       alt=""
                       withPlaceholder={ true }
                       placeholder={ <Loader color={ variables.primaryColor } variant="bars"/> }
                />
                <Stack justify="space-between" className={ styles.firstCardBody }>
                  <Text fz={ 22 } fw={ 700 } c={ variables.primaryColor }
                        lineClamp={ 2 }>{ data.length > 0 ? data[0].name : 'Loading...' }</Text>
                  <Group spacing={ 8 }>
                    <Text fz={ 20 } c={ variables.primaryColor }>
                      Xem chi tiết
                    </Text>
                    <ChevronRight
                      size={ 24 }
                      color={ variables.primaryColor }
                      strokeWidth={ 2 }
                    />
                  </Group>
                </Stack>
              </Stack>
            </Link>
            { slideCardJSX }
          </Group>
        </Stack>
      </Box>
    </>
  );
  return eleHTML;
}

function Gallery2SlideComponent(props) {
  const imageComponentProps = props.imageComponentProps;
  const textComponentProps = props.textComponentProps;

  const imageComponent = (
    <Image src={ imageComponentProps.image }
           width={ imageComponentProps.width }
           height={ imageComponentProps.height }
           alt={ imageComponentProps.alt }
           placeholder={ imageComponentProps.placeholder }
           withPlaceholder={ true }
           radius={ 40 }/>
  );

  const textComponent = (

    <Stack justify={ "flex-start" } spacing={ 124 } className={ styles.gallery2TextComponent }>
      <Text lineClamp={ 3 } fz={ 22 } fw={ 700 } c={ variables.primaryColor }>{ textComponentProps.text }</Text>
      <Group spacing={ 8 }>
        <Text fz={ 20 } c={ variables.primaryColor }>
          Xem chi tiết
        </Text>
        <ChevronRight
          size={ 24 }
          color={ variables.primaryColor }
          strokeWidth={ 2 }
        />
      </Group>
    </Stack>
  );

  const eleHTML = (
      <div className={ styles.item }>
        <Link href={ props.link }>
          <Group spacing={ 0 }>
            { props.isImageSlideCard ? textComponent : imageComponent }
            <div className={ styles.slideCard }>
              { props.isImageSlideCard ? imageComponent : textComponent }
            </div>
          </Group>
        </Link>
      </div>
    )
  ;
  return eleHTML;
}


function Gallery3R(props) {
  let imageJSXArray = [];
  let imagePerRow = props.data.length === 4 ? 2 : 3;

  const currentWidth = props.windowDimension.width;
  const divider = Math.min((currentWidth < 1440) ? 1440 / currentWidth : 1, 1.5);

  if (props.windowDimension.width < 1100) {
    imagePerRow = 2;
  }
  if (props.windowDimension.width < 780) {
    imagePerRow = 1;
  }
  const galleryImage = props.data?.map((dataSingleton, index) => {
    const image = <Image key={ `gallery3-photo-${ index }` } radius={ 20 }
                         style={ {objectFit: 'contain'} } src={ dataSingleton.image } alt={ `photo-${ index }` }
                         height={ 223 }/>

    imageJSXArray.push(image);
    if ((index + 1) % imagePerRow === 0 || index + 1 === props.optionData?.length) {
      const returnJSXArray = imageJSXArray.slice();
      imageJSXArray = [];
      const max_width = (1440 / imagePerRow) * ((returnJSXArray.length) % (imagePerRow + 1));
      return (
        <Center key={ `group-${ index }` }>
          <Flex mih={ 32 }
                style={ {width: max_width} }
                gap="xl"
                justify="center"
                align="center"
                direction="row">
            { returnJSXArray }
          </Flex>
        </Center>

      );
    }

  });
  const eleHTML = (
    <Stack spacing={ 44 / divider }
           style={ {padding: `${ 44 / divider }px ${ 64 / divider }px`} }
           className={ styles.gallery3 }>
      <Text align="center" fz={ 32 / divider } fw={ 700 }>{ props.title }</Text>
      <Stack spacing={ 32 / divider }>
        { galleryImage }
      </Stack>
    </Stack>
  );
  return eleHTML;
}

function Gallery4(props) {
  const [data, setData] = useState([]);
  useEffect(() => {
    loadData({
      url: props.url,
      data: props.optionData,
      query: props.optionQuery,
      jsonata: props.optionJsonata
    }, setData, true);
  }, []);

  const eleHTML = (
    <Stack spacing={ 32 } className={ styles.gallery4 }>
      <Stack spacing={ 16 }>
        <Text className={ globals.heading1 }>{ props.title }</Text>
        <Divider size={ 1 } color={ variables.secondColor } style={ {width: "100px"} }/>
      </Stack>
      <Group spacing={ 32 } className={ styles.gallery4Body }>
        <Link className={ styles.firstCard }
              href={ `${ props.optionData }/${
                data.length > 0 ? data[0].id : "id-0"
              }?type_child=${ props.optionTypeChild }` }>
          <Stack>
            <Image radius={ "40px 40px 0px 0px" } width={ 520 } height={ 385.24 }
                   src={ data.length > 0 ? data[0].image : props.logo } alt=""/>
            <Stack justify="space-between" className={ styles.firstCardBody }>
              <Text fz={ 22 } fw={ 700 } c={ variables.primaryColor }
                    lineClamp={ 2 }>{ data.length > 0 ? data[0].name : 'Loading...' }</Text>
              <Group spacing={ 8 }>
                <Text fz={ 20 } c={ variables.primaryColor }>
                  Xem chi tiết
                </Text>
                <ChevronRight
                  size={ 24 }
                  color={ variables.primaryColor }
                  strokeWidth={ 2 }
                />
              </Group>
            </Stack>
          </Stack>
        </Link>
        <Stack spacing={ 32 }>
          <Gallery4HorizontalCard image={ data.length > 0 ? data[1].image : props.logo }
                                  title={ data.length > 0 ? data[1].name : 'Loading...' }
                                  link={ data.length > 0 ? `${ props.optionData }/${
                                    data[1] ? data[1].id : "id-0"
                                  }?type_child=${ props.optionTypeChild }` : '' }
                                  isImageLeft={ true }/>

          <Gallery4HorizontalCard image={ data.length > 0 ? data[2].image : props.logo }
                                  title={ data.length > 0 ? data[2].name : 'Loading...' }
                                  link={ data.length > 0 ? `${ props.optionData }/${
                                    data[2] ? data[2].id : "id-0"
                                  }?type_child=${ props.optionTypeChild }` : '' }
                                  isImageLeft={ false }/>
        </Stack>
      </Group>
    </Stack>
  );


  return eleHTML;
}

function Gallery4HorizontalCard(props) {
  const imageComponent = <Image radius={ 24 } src={ props.image } alt="" width={ 320 } height={ 320 }/>

  const textComponent = (
    <Stack style={ {padding: "24px", width: "320px"} } spacing={ 124 }>
      <Text lineClamp={ 4 } fz={ 22 } fw={ 700 } c={ variables.primaryColor }>{ props.title }</Text>
      <Group spacing={ 8 }>
        <Text fz={ 20 } c={ variables.primaryColor }>
          Xem chi tiết
        </Text>
        <ChevronRight
          size={ 24 }
          color={ variables.primaryColor }
          strokeWidth={ 2 }
        />
      </Group>
    </Stack>
  );

  const eleHTMl = (
    <>
      <Link href={ props.link }>
        <Group spacing={ 32 } style={ {width: "100%", height: "100%"} }>
          { props.isImageLeft ? imageComponent : textComponent }
          { props.isImageLeft ? textComponent : imageComponent }
        </Group>

      </Link>
    </>
  );

  return eleHTMl;
}

