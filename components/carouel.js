/* eslint-disable react-hooks/exhaustive-deps */
import variables from "/styles/variables.module.scss";
import styles from "/styles/feature.module.scss";
import { Box, Button, Card, Center, Divider, Grid, Group, Image, Loader, Stack, Text, } from "@mantine/core";
import { ButtonBack, ButtonNext, CarouselProvider, DotGroup, Slide, Slider } from 'pure-react-carousel';
import { AlertCircle, ArrowNarrowRight, Clock, CurrencyDollar, Location, } from "tabler-icons-react";
import { Carousel } from "@mantine/carousel";
import { useEffect, useState } from "react";
import Link from "next/link";
import parse from "html-react-parser";
import md from "markdown";
import { getDataFromCollection } from "@/services/services";
import 'pure-react-carousel/dist/react-carousel.es.css';
import dayjs from "dayjs";

export default function _Carousel(props) {
  let eleHTML = <> </>;
  switch (props.display) {
    case "carousel1":
      eleHTML = <Carousel1 { ...props.options } url={ props.url }/>
      break;

    case "carousel2":
      if (props.windowDimension.width >= 1100) {
        eleHTML = <Carousel2 { ...props.options } url={ props.url } background={ props.background }/>;
      } else {
        eleHTML = <Carousel2R { ...props.options } url={ props.url } background={ props.background }
                              windowDimension={ props.windowDimension }/>;
      }
      break;

    case "carousel3":
      eleHTML = <Carousel3 { ...props.options } url={ props.url }/>;
      break;

    case "carousel4":
      eleHTML = <Carousel4 { ...props.options } url={ props.url }/>;
      break;

    case "carousel5":
      if (props.windowDimension.width >= 1000)
        eleHTML = <Carousel5R { ...props.options } url={ props.url }
                              windowDimension={ props.windowDimension }/>;
      else
        eleHTML = <Carousel2R { ...props.options } url={ props.url } background={ props.background }
                              windowDimension={ props.windowDimension }/>;
      break;

    default:
      break;
  }

  return eleHTML;
}

function loadData(loadDataOptions, setDataFunc) {
  const loadDataFunc = async () => await getDataFromCollection(
    loadDataOptions.url,
    loadDataOptions.data,
    loadDataOptions.query ? loadDataOptions.query : "",
    loadDataOptions.jsonata
  );
  loadDataFunc().then(res => {
    if (loadDataOptions.display === "carousel4") {
      const resSorted = res.sort((a, b) => {
        const date1 = new Date(a.date);
        const date2 = new Date(b.date);
        return date2 - date1;
      });
      setDataFunc(resSorted);
    } else {
      setDataFunc(res);
    }
  });
}

function Carousel1(props) {
  const [data, setData] = useState([]);
  useEffect(() => {
    loadData({
      url: props.url,
      data: props.data,
      query: props.query,
      jsonata: props.jsonata,
      display: props.display
    }, setData);

  }, []);

  const eleHTML = (
    <Box
      className={ styles.carousel1 }
      sx={ (theme) => ({
        padding: "0px 64px 56px 64px",
        background: "#F2F2F2",
      }) }
    >
      <Stack>
        <Text align="center" className={ styles.title }>
          { props.title }
        </Text>
        <Center>
          <div className={ styles.boxDivider }>
            <Divider size={ 1 } color={ variables.primaryColor }/>
          </div>
        </Center>
      </Stack>
      <Carousel
        className={ styles.carousel }
        slideSize="25%"
        slideGap={ 48 }
        loop
        align="start"
        slidesToScroll={ 2 }
        controlSize={ 48 }
        mt={ 48 }
      >
        { data?.map((ele, index) => (
          <Carousel.Slide key={ `slide-${ index }` }>
            <Stack className={ styles.slide } spacing={ 0 }>
              <Image
                src={ ele.image }
                withPlaceholder
                placeholder={ <Loader color={ variables.primaryColor } variant="bars"/> }
                alt="job 1"
                height={ 400 }
              />
              <Stack p={ 24 }>
                <div className={ styles.flexBetween }>
                  <Group>
                    <AlertCircle
                      size={ 16 }
                      strokeWidth={ 1.5 }
                      color={ "#F5222D" }
                    />
                    <Text color={ "#F5222D" }>Urgent</Text>
                  </Group>
                  <Group>
                    <Clock
                      size={ 16 }
                      strokeWidth={ 1.5 }
                      color={ "rgba(37, 37, 37, 0.7)" }
                    />
                    <Text>{ ele.expiration_date }</Text>
                  </Group>
                </div>
                <Link
                  href={ `/${ props.data }/${ ele.id }?type_child=${ props.child }` }
                >
                  <Text className={ styles.jobName } lineClamp={ 1 }>
                    { ele.name }12
                  </Text>
                </Link>
                <Group>
                  <Location
                    size={ 24 }
                    strokeWidth={ 1.5 }
                    color={ "rgba(37, 37, 37, 0.7)" }
                  />
                  <Text>{ ele.location }</Text>
                </Group>
                <Group>
                  <CurrencyDollar
                    size={ 24 }
                    strokeWidth={ 1.5 }
                    color={ "rgba(37, 37, 37, 0.7)" }
                  />
                  <Text>{ ele.salary }</Text>
                </Group>
                <Button
                  style={ {
                    background: variables.primaryColor,
                    borderRadius: 36,
                  } }
                >
                  Apply
                </Button>
              </Stack>
            </Stack>
          </Carousel.Slide>
        )) }
      </Carousel>
    </Box>
  );
  return eleHTML;
}

function Carousel2(props) {
  const [data, setData] = useState([]);
  useEffect(() => {
    loadData({
      url: props.url,
      data: props.data,
      query: props.query,
      jsonata: props.jsonata,
      display: props.display
    }, setData);

  }, []);

  const contentJSX = data.map((ele, index) => {
    const cardJSX = (
      <Card
        sx={ {borderColor: '#FFF'} }
        radius={ 32 }
        p={ 24 }
        m={ 0 }
        withBorder
      >
        <Stack spacing={ 24 } style={ {width: 272} }>
          <Image
            radius={ 32 }
            src={ ele.image }
            withPlaceholder
            placeholder={ <Loader color={ variables.primaryColor } variant="bars"/> }
            alt="job 1"
            height={ 240 }
          />
          <Link
            href={ `/${ props.data }/${ ele.id }?type_child=${ props.child }` }
          >
            <Text
              className={ styles.jobName }
              align="center"
              lineClamp={ 1 }
            >
              { ele.name }
            </Text>
          </Link>
        </Stack>
      </Card>
    );

    if (data.length
      <= 3)
      return (
        <div key={ index }>
          { cardJSX }
        </div>
      );
    else
      return (
        <Carousel.Slide key={ index }>
          { cardJSX }
        </Carousel.Slide>
      );
  });

  const eleHTML = (
    <Box
      className={ styles.carousel1 }
      sx={ (theme) => ({
        padding: "44px 64px",
        background: `linear-gradient(to bottom, ${ props.background }20 70%, transparent 0)`,
      }) }
    >
      <Grid>
        <Grid.Col span={ 3 } p={ 0 } m={ 0 }>
          <Stack mt={ -1 } spacing={ 24 }>
            <Text align="left" className={ styles.title }>
              { props.title }
            </Text>
            <Text fz={ 14 } fw={ 400 } color={ 'rgba(21, 20, 57, 0.8)' }>{ props.subtitle }</Text>
          </Stack>
        </Grid.Col>
        <Grid.Col span={ 9 } p={ 0 } m={ 0 }>
          {
            data.length <= 3
              ? <Group spacing={ 32 } position={ "center" }>
                { contentJSX }
              </Group>
              : <Carousel
                className={ styles.carousel }
                slideSize="33.3333333%"
                slideGap={ 32 }
                loop
                align="start"
                slidesToScroll={ 1 }
                controlSize={ 48 }
                controlsOffset={ 0 }
                mt={ 64 }
                sx={ {
                  button: {
                    background: '#FFF',
                    border: "none",
                  },
                  svg: {color: variables.primaryColor, width: "24px", height: "24px"},
                } }
                styles={ {controls: {left: -56, right: -56}} }
              >
                { contentJSX }
              </Carousel>
          }
        </Grid.Col>
      </Grid>
    </Box>
  );
  return eleHTML;
}

function Carousel2R(props) {
  const [data, setData] = useState([]);
  useEffect(() => {
    loadData({
      url: props.url,
      data: props.data,
      query: props.query,
      jsonata: props.jsonata,
      display: props.display
    }, setData);

  }, []);
  const currentWidth = props.windowDimension.width
  const divider = 1100 / currentWidth;

  const carouselSlideJSX = data?.map((ele, index) => (
    <Carousel.Slide key={ `carousel2-${ index }` }>
      <Center>
        <Card
          sx={ {borderColor: variables.primaryColor} }
          radius={ 32 / divider }
          style={ {width: "70%"} }
          withBorder
        >
          <Card.Section>
            <Image
              radius={ 32 / divider }
              src={ ele.image }
              withPlaceholder
              placeholder={ <Loader color={ variables.primaryColor } variant="bars"/> }
              alt="job 1"
              height={ Math.min(currentWidth / 1.5, 400) }
            />
          </Card.Section>
          <Link
            href={ `/${ props.data }/${ ele.id }?type_child=${ props.child }` }
          >
            <Text
              fz={ 40 / divider } fw={ 500 } c={ variables.primaryColor }
              align="center"
              lineClamp={ 1 }
            >
              { ele.name }
            </Text>
          </Link>
        </Card>
      </Center>
    </Carousel.Slide>
  ));

  const eleHTML = (
    <Box
      sx={ (theme) => ({
        padding: 30 / divider,
        background: `linear-gradient(to bottom, ${ props.background }20 70%, transparent 0)`,
      }) }
    >
      <Stack spacing={ 35 / divider }>
        <Text align="center" fz={ currentWidth < 700 ? 28 : 48 } fw={ 700 } c={ variables.primaryColor }>
          { props.title }
        </Text>
        <Center>
          <div style={ {width: "50%"} }>
            <Divider size={ 1 } color={ variables.primaryColor }/>
          </div>
        </Center>
        { currentWidth < 700 ? <> </> : <Text>{ props.subtitle }</Text> }
        <Carousel
          className={ styles.carousel }
          slideSize="100%"
          slideGap={ 32 }
          loop
          align="start"
          slidesToScroll={ 1 }
          controlSize={ 50 / divider }
          controlsOffset={ 0 }
          mt={ 64 / divider }
          sx={ {
            button: {
              background: variables.primaryColor,
              border: "none",
            },
            svg: {color: "#FFF", width: 36 / divider, height: 36 / divider},
          } }
        >
          { carouselSlideJSX }
        </Carousel>
      </Stack>
    </Box>
  );


  return eleHTML;
}

function Carousel3(props) {
  const [data, setData] = useState([]);
  useEffect(() => {
    loadData({
      url: props.url,
      data: props.data,
      query: props.query,
      jsonata: props.jsonata,
      display: props.display
    }, setData);

  }, []);
  const eleHTML = (
    <Box sx={ {padding: "0px 64px"} } className={ `${ styles.carousel1 } ${ styles.carousel3 }` }>
      <Stack>
        <Text align="left" className={ styles.title }>
          { props.title }
        </Text>
        <div className={ styles.boxDivider }>
          <Divider size={ 1 } color={ variables.primaryColor }/>
        </div>
        <Text>{ props.subtitle }</Text>
      </Stack>
      <Carousel
        className={ styles.carousel }
        slideSize="25%"
        slideGap={ 32 }
        loop
        align="start"
        slidesToScroll={ 1 }
        controlSize={ 48 }
        controlsOffset={ 0 }
        mt={ 44 }
        sx={ {
          button: {background: variables.primaryColor, border: "none"},
          svg: {color: "#FFF", width: "24px", height: "24px"},
        } }
        styles={ {controls: {top: '-137px', justifyContent: 'normal', left: 'auto'}, control: {marginLeft: 16}} }
      >
        { data?.map((ele, index) => (
          <Carousel.Slide key={ `carousel2-${ index }` }>
            <Card
              // sx={{ borderColor: variables.primaryColor }}
              radius={ 32 }
              p={ 24 }
              h={ 384 }
              withBorder
              shadow='md'
            >
              <Card.Section>
                <Image
                  src={ ele.image }
                  withPlaceholder
                  placeholder={ <Loader color={ variables.primaryColor } variant="bars"/> }
                  alt="job 1"
                  height={ 192 }
                />
              </Card.Section>
              <Stack spacing={ 16 } p={ 16 }>
                <Link
                  href={ `/${ props.data }/${ ele.id }?type_child=${ props.child }` }
                >
                  <Text
                    className={ styles.carousel3Title }
                    lineClamp={ 1 }
                  >
                    { ele.name }
                  </Text>
                </Link>
                <Text className={ styles.summary } lineClamp={ 3 }>
                  It is a long established fact that a reader will be
                  distracted by the readable content of a page when looking
                  at its layout. The point of using Lorem Ipsum is that
                </Text>
                <Group align="center">
                  <Text className={ styles.readMore }>{ "Xem thêm" }</Text>
                  <ArrowNarrowRight size={ 24 } color={ variables.primaryColor } strokeWidth={ 1.5 }/>
                </Group>
              </Stack>
            </Card>
          </Carousel.Slide>
        )) }
      </Carousel>
    </Box>
  );
  return eleHTML;
}

function Carousel4(props) {
  const [data, setData] = useState([]);
  useEffect(() => {
    loadData({
      url: props.url,
      data: props.data,
      query: props.query,
      jsonata: props.jsonata,
      display: props.display
    }, setData);

  }, []);

  let carouselSlideJSX = <></>;
  carouselSlideJSX = data.map(dataSingleton =>
    <Carousel.Slide key={ dataSingleton.id }>
      <Card shadow="sm" padding="lg" radius="md" withBorder>
        <Card.Section>
          <Image
            src={ dataSingleton.image }
            height={ 200 }
            alt={ dataSingleton.image }
            withPlaceholder
            placeholder={ <Loader color={ variables.primaryColor } variant="bars"/> }
          />
        </Card.Section>

        <Group position="center" mt="md" mb="xs">
          <Text lineClamp={ 1 } weight={ 700 }>{ dataSingleton.excerpt }</Text>

        </Group>

        <Text size="sm" color="dimmed" lineClamp={ 2 }>
          { parse(md.parse(dataSingleton.markdown)) }
        </Text>

        <Group position="center" mt="md" mb="xs">
          <Link
            href={ `${ props.data }/${ dataSingleton.id }?type_child=${ props.child }` }>
            <Text fz={ 20 } c={ variables.primaryColor }>
              Xem chi tiết
            </Text>
          </Link>
        </Group>
      </Card>
    </Carousel.Slide>
  );

  const eleHTML = (
    <Box className={ styles.container }
         sx={ (theme) => ({
           padding: "0px 100px",
         }) }>
      <Carousel
        withIndicators
        height={ "auto" }
        slideSize="33.333333%"
        slideGap="md"
        loop={ false }
        align="start"
        slidesToScroll={ 3 }
        sx={ {
          button: {
            background: variables.primaryColor,
            border: "none",
          },
          svg: {color: "#FFF", width: "24px", height: "24px"},
        } }
      >
        { carouselSlideJSX }
      </Carousel>
    </Box>
  );

  return eleHTML;
}

function Carousel5R(props) {
  const [data, setData] = useState([]);
  useEffect(() => {
    loadData({
      url: props.url,
      data: props.data,
      query: props.query,
      jsonata: props.jsonata,
      display: props.display
    }, setData);

  }, []);

  let carousel5SlideJSX = [];

  const maxSlideVisible = (props.windowDimension.width >= 1200) ? 3 : 2;
  carousel5SlideJSX = data.map((dataSingleton, index) => {
    const expirationDate = dataSingleton.expiration_date ? dayjs(dataSingleton.expiration_date) : "Invalid Date";
    const slideCard = (
      <Stack justify={ 'flex-start' } className={ styles.card } spacing={ 24 }>
        <Image
          src={ dataSingleton.image }
          height={ 344 }
          width={ 400 }
          radius={ "32px 32px 0px 0px" }
          alt={ dataSingleton.image }
          withPlaceholder
          placeholder={ <Loader color={ variables.primaryColor } variant="bars"/> }
        />
        <Stack spacing={ 28 } className={ styles.cardInfo }>
          <Stack spacing={ 16 }>
            <Group position={ "apart" }>
              <Group spacing={ 8 }>
                <Image src="/images/urgentIcon.svg" width={ 16 } alt=""/>
                <Text fw={ 400 } fz={ 14 } color="red">Urgent</Text>
              </Group>
              <Group spacing={ 8 }>
                <Image src="/images/clockIcon.svg" width={ 16 } alt=""/>
                <Text fw={ 400 } fz={ 14 } color="black">{
                  expirationDate.toString() === 'Invalid Date' ? 'Không giới hạn' : expirationDate.format('DD/MM/YYYY')
                }</Text>
              </Group>
            </Group>
            <Group position="apart" mt="md" mb="xs">
              <Text fz={ 20 } fw={ 700 } color={ variables.primaryColor }>{ dataSingleton.name }</Text>

            </Group>

            <Stack spacing={ 12 }>
              <Group spacing={ 8 }>
                <Image src="/images/marker.svg" width={ 20 } alt=""/>
                <Text fz={ 16 } fw={ 400 }>{ dataSingleton.location }</Text>
              </Group>
              <Group spacing={ 8 }>
                <CurrencyDollar size={ 20 } strokeWidth={ 1.5 }/>
                <Text fz={ 16 } fw={ 400 }>{ dataSingleton.salary }</Text>
              </Group>
            </Stack>
          </Stack>

          <Button color="white" className={ styles.applyButton } fullWidth mt="md">
            Apply
          </Button>
        </Stack>
      </Stack>
    );
    if (data.length > maxSlideVisible) {
      return (
        <Slide key={ `slide ${ index }` }>
          { slideCard }
        </Slide>
      );
    } else {
      return (
        <div key={ `slide ${ index }` }>
          { slideCard }
        </div>
      )
    }

  });

  const carouselDotAndButtonJSX = (
    <Group position={ "apart" }>
      <Group align={ "center" }>
        <DotGroup showAsSelectedForCurrentSlideOnly={ true } disableActiveDots={ true }
                  className={ styles.dotButton }>
        </DotGroup>
      </Group>
      <Group spacing={ 32 }>
        <ButtonBack style={ {border: 0} }> <Image src="/images/icons8-back-arrow-50.png"
                                                  width={ 56 }/></ButtonBack>
        <ButtonNext style={ {border: 0} }> <Image src="/images/icons8-forward-button-50.png"
                                                  width={ 56 }/></ButtonNext>
      </Group>
    </Group>
  );

  const carouselContent = data.length > maxSlideVisible
    ? (<CarouselProvider
        visibleSlides={ maxSlideVisible }
        naturalSlideWidth={ 400 }
        naturalSlideHeight={ 592 }
        totalSlides={ data.length }
      >
        <Slider>
          { carousel5SlideJSX }
        </Slider>
        { carouselDotAndButtonJSX }
      </CarouselProvider>
    )
    : (
      <Center>
        <Group spacing={ 32 }>
          { carousel5SlideJSX }
        </Group>
      </Center>
    );

  const eleHTML = (
    <Box
      className={ styles.carousel5 }
      sx={ (theme) => ({
        padding: "44px 64px",

      }) }
    >
      <Stack spacing={ 44 }>
        <Stack spacing={ 16 }>
          <Text align="center" className={ styles.title }>
            { props.title }
          </Text>
          <Center>
            <Divider size={ 0 } className={ styles.boxDivider }/>
          </Center>
        </Stack>

        <Box className={ styles.carousel }>
          { carouselContent }
        </Box>
      </Stack>
    </Box>
  );

  return eleHTML;
}
