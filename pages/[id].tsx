/* eslint-disable react-hooks/exhaustive-deps */
import Head from 'next/head'
import { Stack } from '@mantine/core'
import { useEffect } from 'react'
import _Menu from '../components/menu'
import _Footer from '../components/footer'
import Feature from '../components/feature'
import { getLayout, setHostname } from '@/services/services'
import getCustomHooks from "@/components/customHooks";

export default function Home(props: {
  url: string,
  config: {
    logo: string,
    favicon: string,
    primaryColor: string,
    secondaryColor: string,
    hoverColor: string,
    focusColor: string,
    facebook: string,
    instagram: string,
    twitter: string
    backgroundPrimaryBased: string,
    backgroundLightGrey: string,
    googlePlus: string,
    fabButton: { display: string, options: { backgroundColor: string, href: string, icon: string } },
  },
  headers: Array<{ sort: number, title: string, href: string, status: string }>,
  page: any,
  footers: { display: string, logo: string, data: any, description: string }
}) {
  const useWindowSizeHook = getCustomHooks({type: 'hook1'});
  // @ts-ignore
  const {height: currentHeight, width: currentWidth} = useWindowSizeHook();
  useEffect(() => {
    const root = document.documentElement;
    root?.style.setProperty("--primaryColor", props.config.primaryColor ? props.config.primaryColor : '#fff');
    root?.style.setProperty("--secondaryColor", props.config.secondaryColor ? props.config.secondaryColor : '#fff');
    root?.style.setProperty("--hoverColor", props.config.hoverColor ? props.config.hoverColor : '#fff');
    root?.style.setProperty("--focusColor", props.config.focusColor ? props.config.focusColor : '#fff');
    root?.style.setProperty("max-width", "1440px");
    document.body.style.setProperty("overflow", "hidden");

  }, []);
  return (
    <>
      <Head>
        <title>CMS platform</title>
        <meta name="description" content="Build your web"/>
        <meta name="viewport" content="width=device-width, initial-scale=1"/>
        <link rel="icon" href={ props.config.favicon }/>
      </Head>
      <_Menu display='basic' logo={ props.config.logo }
             data={ props.headers } twitter={ props.config.twitter }
             facebook={ props.config.facebook } googlePlus={ props.config.googlePlus }
             windowDimension = { { width: currentWidth, height: currentHeight } }/>
      { currentWidth < 750 ? <div style={{height: 80 * currentWidth / 750}}></div> : <></> }

      <Stack spacing={ 44 } pb={ 56 }
             style={ {background: '#F2F2F2', minHeight: 'calc(100vh - 250px - 96px)'} }>
        {
          props.page?.body?.map((ele: any, index: number) =>
            <Feature
              image={ props.config.logo }
              url={ props.url }
              key={ `feature-${ index }` }
              display={ ele.display }
              component={ ele.component }
              options={ ele.options }
              primaryColor={ props.config.primaryColor ? props.config.primaryColor : '#fff' }
              secondaryColor={ props.config.secondaryColor ? props.config.secondaryColor : '#fff' }
              background={ props.config.primaryColor ? props.config.primaryColor : '#fff' }
              backgroundPrimaryBased={ props.config.backgroundPrimaryBased }
              backgroundLightGrey={ props.config.backgroundLightGrey }
              windowDimension={ {width: currentWidth, height: currentHeight} }
            />
          )
        }
      </Stack>
      { props.config.fabButton !== null ?
        <Feature component="fab-button" display={ props.config.fabButton.display }
                 options={ props.config.fabButton.options }/>
        : <></>
      }
      <_Footer
        display={ props.footers.display }
        logo={ props.footers.logo }
        data={ props.footers.data }
        facebook={ props.config.facebook }
        instagram={ props.config.instagram }
        twitter={ props.config.twitter }
        description={ props.footers.description }
        windowDimension={ {width: currentWidth, height: currentHeight} }/>
    </>
  )
}

export async function getServerSideProps(context: any) {
  const {req} = context;
  if (req) {
    setHostname(req.headers.host);
  }
  const layout = getLayout(context.resolvedUrl);
  // const data = await getTalentByCode(req.headers.host, 'A6LKT-P444K');
  //   console.log(data)
  return {
    props: {
      url: req.headers.host,
      config: layout.config,
      headers: layout.headers.filter(e => e.status == 'published').sort((a, b) => a.sort - b.sort),
      page: layout.pages,
      footers: layout.footers
    }
  }
}
