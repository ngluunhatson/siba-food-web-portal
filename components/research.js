/* eslint-disable react-hooks/exhaustive-deps */
import {
  Box,
  Center,
  Stack,
  Text,
  TextInput,
  Group,
  Image,
  SimpleGrid,
  Stepper,
  Button,
  Tooltip,
  List,
  Transition,
  Space,
} from "@mantine/core";
import styles from "../styles/feature.module.scss";
import {
  Calendar,
  GenderBigender,
  Mailbox,
  Phone,
  ChevronRight,
} from "tabler-icons-react";
import variables from "../styles/variables.module.scss";
import { useState } from "react";
import { getTalentByCode } from "@/services/services";
import dayjs from "dayjs";
import { _concat } from "lodash";

export default function Research(props) {
  let eleHTML;
  const [code, setCode] = useState();
  const [talent, setTalent] = useState();
  const getTalent = async (code) => {
    if (code) {
      const data = await getTalentByCode(props.url, code);
      setTalent(data);
    }
  };
  switch (props.display) {
    case "research1":
      eleHTML = (
        <Box
          className={styles.research1}
          sx={(theme) => ({
            padding: "56px 64px",
            maxWidth: "70%",
            width: "100%",
            margin: "0 auto",
          })}
        >
          <Center>
            <Stack>
              <Text className={styles.title} align="center">
                {props.options.title}
              </Text>
              <Group>
                <TextInput
                  radius="md"
                  w={600}
                  onChange={(e) => setCode(e.target.value)}
                />
                <Button
                  radius="md"
                  bg={variables.primaryColor}
                  onClick={() => getTalent(code)}
                  sx={{
                    '&:focus': {backgroundColor: variables.focusColor},
                    '&:hover': {backgroundColor: variables.hoverColor},
                  }}
                >
                  Tìm kiếm
                </Button>
              </Group>
            </Stack>
          </Center>
          <Stack
            className={styles.result}
            spacing={32}
            display={talent != null || talent != undefined ? "" : "none"}
          >
            <Group spacing={32}>
              <Image
                src={talent?.talent.basics.image}
                alt="test"
                withPlaceholder
                placeholder={<Image src={props.image} alt="" />}
                width={300}
                height={260}
                radius={32}
              />
              <Stack spacing={32}>
                <Text fw={700} fz={24} c={variables.primaryColor} lineClamp={1}>
                  {talent?.talent.basics.name}
                </Text>
                <SimpleGrid cols={2} spacing={36}>
                  <Group spacing={24}>
                    <Calendar
                      size={24}
                      strokeWidth={1.5}
                      color={variables.primaryColor}
                    />
                    <Text>
                      {dayjs(talent?.talent.basics.dob).format("DD/MM/YYYY")}
                    </Text>
                  </Group>
                  <Group spacing={24}>
                    <GenderBigender
                      size={24}
                      strokeWidth={1.5}
                      color={variables.primaryColor}
                    />
                    <Text>
                      {talent?.talent.basics.gender == "male"
                        ? "Nam"
                        : talent?.talent.basics.gender == "female"
                        ? "Nữ"
                        : "Khác"}
                    </Text>
                  </Group>
                  <Group spacing={24}>
                    <Mailbox
                      size={24}
                      strokeWidth={1.5}
                      color={variables.primaryColor}
                    />
                    <Text>{talent?.talent.basics.email}</Text>
                  </Group>
                  <Group spacing={24}>
                    <Phone
                      size={24}
                      strokeWidth={1.5}
                      color={variables.primaryColor}
                    />
                    <Text>{talent?.talent.basics.phone}</Text>
                  </Group>
                </SimpleGrid>
              </Stack>
            </Group>
            <Stack>
              <Text fw={500} fz={22} c={variables.primaryColor} lineClamp={1}>
                {talent?.recruitmentPlan.name}
              </Text>
              <Stepper
                iconSize={48}
                active={talent?.currentStage - 1}
                color={variables.primaryColor}
              >
                <Stepper.Step label="Xét duyệt CV" description="Thông qua" />
                {talent?.recruitmentPlan.stages.map((ele, index) => (
                  <Stepper.Step
                    key={`research-${index}`}
                    label={ele.name}
                    description={
                      <Tooltip
                        p={16}
                        color={"white"}
                        style={{
                          border: `1px solid ${variables.primaryColor}`,
                          borderRadius: 32,
                        }}
                        label={ele.schedules.map((schedule, i) => (
                          <List
                            key={`research-schedule-${i}`}
                            spacing="sm"
                            icon={
                              <ChevronRight
                                strokeWidth={1.5}
                                colot={"white"}
                                size={16}
                              />
                            }
                          >
                            <List.Item>
                              <Stack spacing={2}>
                                <Text>{schedule.title}</Text>
                                <Group>
                                  <Text>{""}</Text>
                                  <Space w={5} />
                                  <List>
                                    <List.Item>
                                      {dayjs(schedule.startTime).format(
                                        "HH:mm DD/MM/YYYY"
                                      )}
                                    </List.Item>
                                    <List.Item>
                                      {schedule.scheduleMethod}
                                    </List.Item>
                                    <List.Item>
                                      {schedule.information || "Đang cập nhật"}
                                    </List.Item>
                                  </List>
                                </Group>
                              </Stack>
                            </List.Item>
                          </List>
                        ))}
                      >
                        <Text>{"Chi tiết"}</Text>
                      </Tooltip>
                    }
                  />
                ))}
                <Stepper.Step label="Kết quả" description="Đang cập nhật" />
              </Stepper>
            </Stack>
          </Stack>
        </Box>
      );
      break;
    // {ele.schedules.map((schedule, i) => (
    //   <Stack w={200} spacing={4} key={`research-schedule-${i}`}>
    //     <Text>
    //       {dayjs(schedule.startTime).format("HH:mm DD/MM/YYYY")}
    //     </Text>
    //   </Stack>
    // ))}

    default:
      break;
  }
  return eleHTML;
}
