/* eslint-disable react-hooks/exhaustive-deps */
import Head from 'next/head'
import { Center, Loader, Image, Stack } from '@mantine/core'
import { useEffect } from 'react'
import { getLayout } from '@/services/services'
import Router from 'next/router'

export default function Index(props: {config: {logo: string, favicon: string, primaryColor: string, secondaryColor: string}, page: {href: string}}) {
  useEffect(() => {
    const root = document.documentElement;
    root?.style.setProperty("--primaryColor", props.config.primaryColor);
    root?.style.setProperty("--secondaryColor", props.config.secondaryColor);
    // root?.style.setProperty("--padding", '64px');
    // root?.style.setProperty("--spacing", '112px');

    const {pathname} = Router;
    if(pathname === '/' ){
        Router.push(props.page.href)
    }
  }, [])
  return (
    <>
      <Head>
        <title>CMS platform</title>
        <meta name="description" content="Build your web" />
        <meta name="viewport" content="width=device-width, initial-scale=1" />
        <link rel="icon" href={props.config.favicon} />
      </Head>
      <Center style={{height: '100vh'}}>
        <Stack>
        <Center><Image src={props.config.logo} alt='logo' style={{maxWidth: 500}} /></Center>
        <Center><Loader variant="dots" color={props.config.primaryColor} /></Center>
        </Stack>
      </Center>
    </>
  )
}
export async function getServerSideProps() {
  const config = getLayout('all');
  return {
    props: {
      config: config.config,
      page: config.headers.find(e => e.sort == 1)
    }
  }
}
