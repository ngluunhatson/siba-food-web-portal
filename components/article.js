/* eslint-disable react-hooks/exhaustive-deps */
import { Box, Card, Divider, Group, Image, Loader, Stack, Text } from "@mantine/core";
import styles from "../styles/feature.module.scss";
import { Clock, CurrencyDollar, Location, Tag, User } from "tabler-icons-react";
import dayjs from "dayjs";
import parse from "html-react-parser";
import { useEffect, useState } from "react";
import variables from "../styles/variables.module.scss";
import { getDataFromCollection } from "../services/services";
import { useRouter } from "next/router";
import Link from "next/link";
import * as md from "markdown";

export default function Article(props) {
  const router = useRouter();
  const [dataRelate, setDataRelate] = useState({
    category: [],
    excerpt: '',
    id: '',
    image: '',
    markdown: '',
    subtitle: '',
    title: '',
    ts: '',
    type: '',
  });
  useEffect(() => {
    if (props.display === "type1") {
      const loadData = async () => {
        const res = await getDataFromCollection(
          props.url,
          router.query.id,
          "",
          "$"
        );
        setDataRelate(res);
      };
      loadData();
    }
    if (props.display === "type2") {
      const loadData = async () => {
        const res = await getDataFromCollection(
          props.url,
          '/' + router.query.id,
          router.query.child,
          "$"
        );
        setDataRelate(res);
      };
      loadData();
    }
  }, []);
  let eleHTML;
  switch (props.display) {
    case "type1": // dành cho hiển thị detail recruitment
      eleHTML = (
        <Box
          className={ styles.detail }
          sx={ () => ({
            padding: "56px 64px",
            background: "#FFF",
            maxWidth: "70%",
            margin: "0 auto",
          }) }
        >
          <Text className={ styles.title }>{ props.data.name }</Text>
          <Group py={ 32 } spacing={ 24 }>
            <Group>
              <Clock
                size={ 24 }
                strokeWidth={ 1.5 }
                color={ "rgba(37, 37, 37, 0.7)" }
              />
              <Text>
                { props.data.expiration_date != null
                  ? dayjs(props.data.expiration_date).format("DD/MM/YYYY")
                  : "Không giới hạn thời gian" }
              </Text>
            </Group>
            <Group>
              <Location
                size={ 24 }
                strokeWidth={ 1.5 }
                color={ "rgba(37, 37, 37, 0.7)" }
              />
              <Text>{ props.data.location }</Text>
            </Group>
            <Group>
              <CurrencyDollar
                size={ 24 }
                strokeWidth={ 1.5 }
                color={ "rgba(37, 37, 37, 0.7)" }
              />
              <Text>
                { props.data.maxSalary <= 0
                  ? "Thoả thuận"
                  : props.data.maxSalary }
              </Text>
            </Group>
          </Group>
          <Stack spacing={ 24 }>
            { props.data.description.map((ele, index) => (
              <div key={ `description-${ index }` }>
                <Text className={ styles.subtitle } fz={ 20 } fw={ 500 } mb={ 16 }>
                  { ele.name }
                </Text>
                <Text lh={ "32px" }>{ parse(ele.content) }</Text>
              </div>
            )) }
          </Stack>
          <Divider my={ 32 }/>
          {/* Chưa cấu hình được subtitle */ }
          <Stack mb={ 32 }>
            <Text className={ styles.subtitle }>Công việc liên quan</Text>
            <div className={ styles.divider }>
              <Divider size={ 1 } color={ variables.primaryColor }/>
            </div>
          </Stack>
          <Stack>
            { dataRelate?.map((ele, index) => (
              <Card shadow="sm" radius={ 32 } key={ `dataRelate-${ index }` }>
                <Group noWrap>
                  <Image
                    radius={ 32 }
                    src={ ele.image }
                    withPlaceholder
                    placeholder={ <Image src={ props.image } alt=""/> }
                    width={ 200 }
                    height={ 150 }
                    alt={ ele.image }
                  />
                  <Stack>
                    <Link href={ `/${ router.query.id }/${ ele.id }?type` }>
                      <Text lineClamp={ 2 } className={ styles.jobName }>
                        { ele.name }
                      </Text>
                    </Link>
                    <Group spacing={ 24 }>
                      <Group>
                        <Clock
                          size={ 24 }
                          strokeWidth={ 1.5 }
                          color={ "rgba(37, 37, 37, 0.7)" }
                        />
                        <Text>
                          { ele.expiration_date != null
                            ? dayjs(ele.expiration_date).format("DD/MM/YYYY")
                            : "Không giới hạn thời gian" }
                        </Text>
                      </Group>
                      <Group>
                        <Location
                          size={ 24 }
                          strokeWidth={ 1.5 }
                          color={ "rgba(37, 37, 37, 0.7)" }
                        />
                        <Text>{ ele.location }</Text>
                      </Group>
                      <Group>
                        <CurrencyDollar
                          size={ 24 }
                          strokeWidth={ 1.5 }
                          color={ "rgba(37, 37, 37, 0.7)" }
                        />
                        <Text>
                          { ele.maxSalary <= 0 ? "Thoả thuận" : ele.maxSalary }
                        </Text>
                      </Group>
                    </Group>
                  </Stack>
                </Group>
              </Card>
            )) }
          </Stack>
        </Box>
      );
      break;
    // hiển thị detail cho news
    case "type2":
      eleHTML = <Article2R url={ props.url } background={ props.background } windowDimension={ props.windowDimension }/>;
      break;

    default:
      break;
  }
  return eleHTML || <></>;
}

function Article2R(props) {
  const router = useRouter();
  const [data, setData] = useState({
    category: [],
    excerpt: '',
    id: '',
    image: '',
    markdown: '',
    subtitle: '',
    title: '',
    ts: '',
    type: '',
  });
  const currentWidth = props.windowDimension.width;
  const divider = (currentWidth < 1440 ? 1440 / currentWidth : 1);
  useEffect(() => {
    return () => {
      const loadData = async () => await getDataFromCollection(
        props.url,
        '/' + router.query.id,
        router.query.child,
        "$"
      );
      loadData().then(res => setData(res));
    }
  }, []);
  const eleHTML = (
    <Stack spacing={ 44 / Math.min(divider, 2) } style={ {
      backgroundColor: props.background,
      padding: `${ currentWidth >= 750 ? "44px 120px" : "8px 12px" }`
    } }>
      <Image radius={ 32 } width={ "100%" } height={ 600 / Math.min(divider, 2) }
             src={ data.image } alt="news_image" withPlaceholder
             placeholder={ <Loader color={ variables.primaryColor } variant="bars"/> }/>
      <Stack spacing={ 32 / Math.min(divider, 1.2) }>
        <Stack spacing={ 16 / Math.min(divider, 1.2) }>
          <Text ta={ "left" } c={ variables.primaryColor } fw={ 700 }
                fz={ 32 / Math.min(divider, 1.8) }>{ data.title }</Text>
          <Group spacing={ 16 }>
            <Group spacing={ 8 }>
              <Clock color="black" size={ 16 / Math.min(divider, 1.2) } strokeWidth={ 1.5 }/>
              <Text fz={ 16 / Math.min(divider, 1.2) } fw={ 400 }>{ dayjs(data.ts).format("DD/MM/YYYY") }</Text>
            </Group>
            <Group spacing={ 8 }>
              <User color="black" size={ 16 / Math.min(divider, 1.2) } strokeWidth={ 1.5 }/>
              <Text fz={ 16 / Math.min(divider, 1.2) } fw={ 400 }>Admin</Text>
            </Group>
            <Group spacing={ 8 }>
              <Tag color="black" size={ 16 / Math.min(divider, 1.2) } strokeWidth={ 1.5 }/>
              <Text fz={ 16 / Math.min(divider, 1.2) } fw={ 400 }>Fitness</Text>
            </Group>
          </Group>
        </Stack>
        <div style={ {width: "100%"} }>
          <Divider size={ 1 } color={ variables.primaryColor }/>
        </div>
        <Text fz={ 16 / Math.min(divider, 1.2) } lh={ 2 }>
          { data.type === 'markdown' ? parse(md.parse(data.markdown)) : data.markdown }
        </Text>
      </Stack>
    </Stack>
  );
  return eleHTML;
}
