import variables from "/styles/variables.module.scss";
import styles from "/styles/feature.module.scss";
import { Box, Button, Center, Group, NativeSelect, Stack, Text, TextInput } from "@mantine/core";
import { Briefcase, CurrencyDollar, Location } from "tabler-icons-react";
import { useState } from "react";

export default function Search(props) {
  let eleHTML;
  switch (props.display) {
    case "search1":
      eleHTML = <Search1/>;
      break;
    case "search2":
      return <Search2 handleSearchClick={ props.options.handleSearch } windowDimension={ props.windowDimension }/>
    default:
      break;
  }

  return eleHTML;
}

function Search1(props) {
  const eleHTML = (
    <Box
      className={ styles.search }
      sx={ (theme) => ({
        height: 248,
        background: "#F2F2F2",
        padding: "56px 64px",
        display: "flex",
        justifyContent: "center",
      }) }
    >
      <Group
        spacing={ 44 }
        noWrap
        style={ {background: "#fff", borderRadius: 100, width: "100%"} }
        position="center"
        align='center'
        px={ 44 }
      >
        <Stack spacing={ 16 }>
          <Text className={ styles.label }>Jobs</Text>
          <TextInput
            rightSection={
              <Briefcase
                size={ 24 }
                stoke={ 1.5 }
                color={ variables.primaryColor }
              />
            }
            className={ styles.input }
            variant="unstyled"
            placeholder="Fronend Dev, Full stack,..."
          ></TextInput>
        </Stack>
        <Stack spacing={ 16 }>
          <Text className={ styles.label }>Where</Text>
          <TextInput
            className={ styles.input }
            variant="unstyled"
            placeholder="Where 're you now?"
            rightSection={
              <Location
                size={ 24 }
                stoke={ 1.5 }
                color={ variables.primaryColor }
              />
            }
          ></TextInput>
        </Stack>
        <Stack spacing={ 16 }>
          <Text className={ styles.label }>Salary range?</Text>
          <TextInput
            className={ styles.input }
            variant="unstyled"
            placeholder="Salary range"
            rightSection={
              <CurrencyDollar
                size={ 24 }
                stoke={ 1.5 }
                color={ variables.primaryColor }
              />
            }
          ></TextInput>
        </Stack>
        <Button
          radius={ 40 }
          sx={ {
            "&:hover": {backgroundColor: variables.hoverColor},
            width: 336,
            height: "auto",
            background: variables.primaryColor,
            fontWeight: 400,
            fontSize: 24,
            padding: "19px 0px",
            margin: 0,
          } }
        >
          Search
        </Button>
      </Group>
    </Box>
  );
  return eleHTML;
}

function Search2(props) {
  const [data1, setData1] = useState('Tất cả');
  const [data2, setData2] = useState('Tất cả');

  const currentWidth = props.windowDimension.width;

  let eleHTML = <></>;
  const selectData1 = (
    <NativeSelect
      value={ data1 }
      onChange={ e => setData1(e.target.value) }
      data={ ['Tất cả', 'Khu vực miền Bắc', 'Khu vực miền Nam'] }
      rightSection={ <Location
        size={ 24 }
        stoke={ 1.5 }
        color={ variables.primaryColor }
      />
      }
      placeholder="Chọn khối ngành..."
    />
  );

  const selectData2 = (
    <NativeSelect
      value={ data2 }
      onChange={ e => setData2(e.target.value) }
      data={ ['Tất cả', 'Khối ngành văn phòng', 'Khối ngành nhà máy'] }
      rightSection={ <Briefcase
        size={ 24 }
        stoke={ 1.5 }
        color={ variables.primaryColor }
      />
      }
      placeholder="Chọn khối ngành..."
    />
  );
  if (currentWidth >= 1000) {
    eleHTML = (
      <Box
        className={ styles.search2 }
        sx={ (theme) => ({
          padding: `${ 44 }px ${ 48 }px 0px ${ 44 }px`
        }) }
      >
        <Group className={ styles.searchField }
               spacing={ 56 }
               noWrap
               position="apart"
               align='center'
               style={ {
                 height: 136,
                 padding: `${ 32 }px ${ 48 }px`
               } }

        >
          <Stack spacing={ 10 } style={ {width: "30%"} }>
            <Text fz={ 20 } className={ styles.label }>Khu Vực</Text>

            { selectData1 }

          </Stack>
          <Stack spacing={ 10 } style={ {width: "30%"} }>
            <Text fz={ 20 } className={ styles.label }>Khối ngành</Text>

            { selectData2 }

          </Stack>
          <Stack style={ {width: "20%"} }>
            <Button
              radius={ 40 }
              onClick={ () => props.handleSearchClick(data1, data2) }
              sx={ {
                "&:hover": {backgroundColor: variables.hoverColor},
                width: "100%",
                height: "auto",
                background: variables.primaryColor,
                fontWeight: 500,
                fontSize: 20,
                padding: "18px 0px",
              } }
            >
              Tìm kiếm
            </Button>
          </Stack>
        </Group>
      </Box>
    );
  } else {
    eleHTML = (
      <Box
        className={ styles.search2 }
        sx={ (theme) => ({
          padding: `20px 24px 0px 20px`,

        }) }>
        <Center style={ {background: 'white', borderRadius: 20, padding: 40} }>
          <Stack spacing={ 40 } style={ {width: '100%'} }>
            <Stack spacing={ 10 } style={ {width: "100%"} }>
              <Text fz={ 20 } c={ variables.primaryColor } className={ styles.label }>Khu Vực</Text>
              { selectData1 }
            </Stack>
            <Stack spacing={ 10 } style={ {width: "100%"} }>
              <Text fz={ 20 } c={ variables.primaryColor } className={ styles.label }>Khối ngành</Text>

              { selectData2 }

            </Stack>

            <Center>
              <Button
                radius={ 40 }
                onClick={ () => props.handleSearchClick(data1, data2) }
                sx={ {
                  "&:hover": {backgroundColor: variables.hoverColor},
                  width: "70%",
                  height: "auto",
                  background: variables.primaryColor,
                  fontWeight: 500,
                  fontSize: 20,
                  padding: "12px 0px",
                } }
              >
                Tìm kiếm
              </Button>
            </Center>
          </Stack>
        </Center>
      </Box>
    );

  }
  return eleHTML;
}
