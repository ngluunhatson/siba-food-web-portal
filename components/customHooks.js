import { useEffect, useState } from "react";

export default function getCustomHooks(options) {
  let func = () => {};

  switch(options.type) {
    case 'hook1':
      func = useWindowSize;
      break;
    default:
      break;
  }

  return func;

}

function useWindowSize() {
  const [windowSize, setWindowSize] = useState({
    width: undefined,
    height: undefined,
  });

  useEffect(() => {

    function handleResize() {
      // Set window width/height to state
      setWindowSize({
        width: window.innerWidth,
        height: window.innerHeight,
      });
    }

    window.addEventListener("resize", handleResize);
    handleResize();
    return () => window.removeEventListener("resize", handleResize);
  }, []);
  return windowSize;
}
