import { useState } from "react";
import Feature from "@/components/feature";

export default function Switcher(props) {
  let eleHTML = <></>;
  switch (props.display) {
    case 'switcher1':
      eleHTML = <Switcher1 componentDefault={ props.options.componentDefault }
                           componentChange={ props.options.componentChange }
                           componentController={ props.options.componentController }
                           windowDimension={ props.windowDimension }/>;
      break;
    default:
      break;
  }
  return eleHTML;
}

function ControllerElement(props) {
  let eleHTML = <></>
  switch (props.componentController.component) {
    case 'search':
      eleHTML = <Feature
        display={ props.componentController.display }
        component={ props.componentController.component }
        options={ {handleSearch: props.handleControl} }
        windowDimension={ props.windowDimension }
      />
      break;
    default:
      break;
  }
  return eleHTML;
}

function Switcher1(props) {
  const [componentObject, setComponentObject] = useState(props.componentDefault);
  const handleControl = (data1, data2) => {
    if (data1 !== 'Tất cả' || data2 !== 'Tất cả') {
      setComponentObject(props.componentChange);
    } else {
      setComponentObject(props.componentDefault);
    }
  }
  const tempEleHTML = <Feature
    component={ componentObject.component }
    display={ componentObject.display }
    options={ componentObject.options }
    windowDimension={ props.windowDimension }
  />;

  const eleHTML = (
    <>
      <ControllerElement handleControl={ handleControl }
                         componentController={ props.componentController }
                         windowDimension={ props.windowDimension }/>
      { tempEleHTML }
    </>
  );

  return eleHTML;
}
