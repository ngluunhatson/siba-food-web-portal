import { Box, Divider, Group, Image, Stack, Text } from "@mantine/core";
import variables from "/styles/variables.module.scss";
import { BrandFacebook, BrandInstagram, BrandTwitter } from 'tabler-icons-react';
import Link from 'next/link';

export default function Footer(props) {
  let eleHTML = <></>;

  switch (props.display) {
    case 'basic':
      eleHTML = <FooterBasic data={ props.data }
                             logo={ props.logo }
                             facebook={ props.facebook }
                             twitter={ props.twitter }
                             instagram={ props.instagram }/>
      break;

    case 'footer1':
      eleHTML = <Footer1R data={ props.data }
                         logo={ props.logo }
                         facebook={ props.facebook }
                         twitter={ props.twitter }
                         instagram={ props.instagram }
                         windowDimension={ props.windowDimension }/>
      break;
    default:
      break;
  }

  return eleHTML;
}

function FooterBasic(props) {
  const eleHTML = (
    <>
      <Box sx={ (theme) => ({padding: '64px', height: 250}) }>
        <Group position='apart' align='center'>
          <Image src={ props.logo } alt='logo' width={ 115 }/>
          <Stack spacing={ 32 }>
            <Group position='apart'>
              {
                props.data.map((ele, index) =>
                  <Link key={ `footer-${ index }` } href={ ele.href }><Text fw={ 20 }
                                                                            ta='center'>{ ele.title }</Text></Link>
                )
              }
            </Group>
            <Divider size={ 1 }/>
            <Text fw={ 20 } ta='center' lineClamp={ 2 } w={ 600 }>{ props.description }</Text>
          </Stack>
          <Group align='center' spacing={ 16 }>
            <Link href={ props.facebook }><BrandFacebook size={ 40 } strokeWidth={ 1 }
                                                         color={ variables.primaryColor }/></Link>
            <Link href={ props.instagram }><BrandInstagram size={ 40 } strokeWidth={ 1 }
                                                           color={ variables.primaryColor }/></Link>
            <Link href={ props.twitter }><BrandTwitter size={ 40 } strokeWidth={ 1 }
                                                       color={ variables.primaryColor }/></Link>
          </Group>
        </Group>
      </Box>

    </>
  );
  return eleHTML;
}

function Footer1R(props) {
  const currentWidth = props.windowDimension.width;
  const divider = Math.min((currentWidth < 1440) ? 1440 / currentWidth : 1, 3.2);

  const dataFooter = props.data.map((dataSingleton, index) => {
    const dataChildFooter = dataSingleton.childData.map((childTextDataSingleton, indexChild) =>
      <Text fz={ 16 / divider } key={ `text-data-${ indexChild }` }>{ childTextDataSingleton }</Text>
    );
    return (
      <Stack key={ `stack-data-${ index }` } spacing={ 8 / divider }>
        <Text fz={ 16 / divider } fw={ 700 }>{ dataSingleton.childName }</Text>
        { dataChildFooter }
      </Stack>
    );
  });

  const eleHTML =
    (
      <Group position='apart' align='center'
             style={ {padding: `${ 32 / divider }px ${ 64 / divider }px`} }>
        <Image src={ props.logo } alt='logo' width={ 200 / divider }/>

        <Group spacing={ 44 / divider }>
          { dataFooter }
        </Group>

        <Group align='center' spacing={ 16 / divider }>
          <a target={ "_blank" } rel={ "noreferrer" }
             href={ props.facebook }><BrandFacebook size={ 24 / divider }
                                                    strokeWidth={ 1 }
                                                    color={ 'black' }/></a>
          <a target={ "_blank" } rel={ "noreferrer" }
             href={ props.instagram }><BrandInstagram size={ 24 / divider }
                                                      strokeWidth={ 1 }
                                                      color={ 'black' }/></a>
          <a target={ "_blank" } rel={ "noreferrer" }
             href={ props.twitter }><BrandTwitter size={ 24 / divider } strokeWidth={ 1 }
                                                  color={ 'black' }/></a>
        </Group>
      </Group>
    );
  return eleHTML;
}
