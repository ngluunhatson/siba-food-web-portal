import '@/styles/globals.css'
import type { AppProps } from 'next/app'
import { MantineProvider, Stack, Group, Image, Text } from '@mantine/core'
import { NotificationsProvider } from '@mantine/notifications';
import { ModalsProvider, ContextModalProps, closeAllModals } from '@mantine/modals';
import { Player } from '@lottiefiles/react-lottie-player';
import { useClickOutside } from '@mantine/hooks';

export default function App({ Component, pageProps }: AppProps) {
  const ref = useClickOutside(closeAllModals);
  const successModal = ({ context, id, innerProps }: ContextModalProps<{ text: string, content: string, code: string, color: string }>) => (
    <>
      <Group noWrap ref={ref} h={400} position='center'>
        <Player style={{width: 400}} src='https://assets4.lottiefiles.com/private_files/lf30_nsqfzxxx.json' autoplay keepLastFrame={true} />
        <Stack>
          <Text fw={700} fz={26} c={innerProps.color}>{innerProps.text}</Text>
          <Text fz={20}>{innerProps.content}</Text>
          {
            (innerProps.code != null || innerProps.code != undefined)
            ?
            <Stack >
              <Text fz={20}>{'Mã số tra cứu ứng viên vủa bạn là:'}</Text>
              <Text fz={30} fw={700} c={innerProps.color} align='center'>{innerProps.code}</Text>
            </Stack>
            : <></>
          }
        </Stack>
      </Group>
    </>
  );
  return (
    <MantineProvider theme={{ fontFamily: 'Noto Sans, Avenir', globalStyles: (theme) => ({ 'body': { maxWidth: 1980, margin: '0 auto', overflow: 'scroll' } }) }} withGlobalStyles withNormalizeCSS>
      <ModalsProvider modalProps={{size: '50%', radius: 32, withCloseButton: false}} modals={{ success: successModal }}>
      <NotificationsProvider position="top-right" zIndex={2077}>
        <Component {...pageProps} />
      </NotificationsProvider>
      </ModalsProvider>
    </MantineProvider>
  );
}
