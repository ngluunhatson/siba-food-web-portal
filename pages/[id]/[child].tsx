/* eslint-disable react-hooks/exhaustive-deps */
import Head from 'next/head'
import { Stack } from '@mantine/core'
import { useEffect } from 'react'
import _Menu from '@/components/menu'
import _Footer from '@/components/footer'
import Article from '@/components/article'
import { getDataFromCollection, getLayout, setHostname } from '@/services/services'
import { useRouter } from 'next/router'
import Feature from "@/components/feature";
import getCustomHooks from "@/components/customHooks";

export default function ChildPage(props: {
  url: string,
  config: {
    logo: string,
    favicon: string,
    primaryColor: string,
    secondaryColor: string,
    hoverColor: string,
    focusColor: string,
    backgroundLightGrey: string,
    facebook: string,
    instagram: string,
    googlePlus: string,
    twitter: string
    fabButton: { display: string, options: { backgroundColor: string, href: string, icon: string } },
  },
  headers: Array<{ sort: number, title: string, href: string, status: string }>,
  data: any,
  footers: { display: string, logo: string, data: any, description: string }
}) {
  const useWindowSizeHook = getCustomHooks({type: 'hook1'});
  // @ts-ignore
  const {height: currentHeight, width: currentWidth} = useWindowSizeHook();
  useEffect(() => {
    const root = document.documentElement;
    root?.style.setProperty("--primaryColor", props.config.primaryColor ? props.config.primaryColor : '#fff');
    root?.style.setProperty("--secondaryColor", props.config.secondaryColor ? props.config.secondaryColor : '#fff');
    root?.style.setProperty("max-width", "1440px");
    document.body.style.setProperty("overflow", "hidden");
  }, []);
  const router = useRouter();
  let eleHTML = <></>;
  switch (router.query.type_child) {
    case 'type1': // hiển thị detail cho recruitment
      eleHTML = <Article url={ props.url }
                         image={ props.config.logo }
                         display={ router.query.type_child }
                         data={ props.data }
                         windowDimension={ {width: currentWidth, height: currentHeight} }></Article>
      break;

    // hiển thị detail cho news
    case 'type2':
      eleHTML = <Article url={ props.url }
                         image={ props.config.logo }
                         display={ router.query.type_child }
                         background={ props.config.backgroundLightGrey }
                         windowDimension={ {width: currentWidth, height: currentHeight} }></Article>
      break;

    default:
      break;
  }

  return (
    <>
      <Head>
        <title>CMS platform</title>
        <meta name="description" content="Build your web"/>
        <meta name="viewport" content="width=device-width, initial-scale=1"/>
        <link rel="icon" href={ props.config.favicon }/>
      </Head>
      <_Menu display='basic' logo={ props.config.logo } data={ props.headers }
             facebook={ props.config.facebook } googlePlus={ props.config.googlePlus }
             twitter={ props.config.twitter }
             windowDimension={ {width: currentWidth, height: currentHeight} }></_Menu>
      { currentWidth < 750 ? <div style={{height: 80 * currentWidth / 750}}></div> : <></> }
      <Stack spacing={ 56 } pb={ 56 } style={ {background: '#FFFFFF', minHeight: 'calc(100vh - 250px - 96px)'} }>
        { eleHTML }
      </Stack>

      { props.config.fabButton !== null ?
        <Feature component="fab-button" display={ props.config.fabButton.display }
                 options={ props.config.fabButton.options }/>
        : <></>
      }
      <_Footer display={ props.footers.display } logo={ props.footers.logo } data={ props.footers.data }
               facebook={ props.config.facebook } instagram={ props.config.instagram } twitter={ props.config.twitter }
               description={ props.footers.description }
               windowDimension={ {width: currentWidth, height: currentHeight} }/>
    </>
  )
}

export async function getServerSideProps(context: any) {
  const {req, params} = context;
  // console.log(context)
  if (req) {
    setHostname(req.headers.host);
  }
  const layout = getLayout(context.resolvedUrl);
  const data = params.id == 'articles' ? [] : await getDataFromCollection(req.headers.host, `${ params.id }/${ params.child }`, '', '$');
  return {
    props: {
      url: req.headers.host,
      config: layout.config,
      headers: layout.headers.filter(e => e.status == 'published').sort((a, b) => a.sort - b.sort),
      data: data,
      footers: layout.footers
    }
  }
}
