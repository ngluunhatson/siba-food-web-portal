import axios from 'axios'
import jsonata from 'jsonata'

var convert = require('color-convert');

const logoLink = "/images/sibafood-logo.png";

const faviconLink = "/images/sibafood-favicon.png";

const layout = {
  config: {
    logo: logoLink,
    favicon: faviconLink,
    primaryColor: '#008848',
    secondaryColor: '#79BD9C',
    backgroundPrimaryBased: 'rgba(0, 136, 72, 0.1)',
    backgroundLightGrey: '#F2F6F9',
    facebook: 'https://www.google.com',
    instagram: 'https://www.google.com',
    twitter: 'https://www.google.com',
    googlePlus: 'https://www.google.com',
    fabButton: {
      display: 'fab1',
      options: {
        backgroundColor: '#008848',
        href: '/apply',
        icon: '/images/fabIconAdd.svg'
      }
    },
  },
  headers: [
    {
      "sort": 1,
      "title": "Trang chủ ",
      "href": "/home",
      "status": "published"
    },
    {
      "sort": 2,
      "title": "Giới thiệu",
      "href": "/introduction",
      "status": "published"
    },
    {
      "sort": 3,
      "title": "Tuyển dụng",
      "href": "/recruitments",
      "status": "published"
    },
    {
      "sort": 4,
      "title": "Tin tức - Sự kiện",
      "href": "/news",
      "status": "published"
    },
    {
      "sort": 5,
      "title": "Tra cứu ứng tuyển",
      "href": "/research",
      "status": "published"
    },
    // {
    //   "sort": 6,
    //   "title": "Đăng ký ứng tuyển ",
    //   "href": "/apply",
    //   "status": "published"
    // },

  ],
  pages: [
    {
      name: 'Trang chủ',
      title: 'Công ty Hệ thống Thông tin FPT',
      description: 'FPT IS mong muốn trở thành công ty cung cấp các giải pháp phần mềm và dịch vụ công nghệ thông tin toàn cầu, luôn luôn sáng tạo các giá trị vì khách hàng, đem lại cuộc sống hạnh phúc cho toàn thể các thành viên, đóng góp cho cộng đồng.',
      thumbnail: '',
      href: '/home',
      body: [
        {
          component: 'hero',
          display: 'hero5',
          options: {
            image: "/images/heroImage5.png",
            backgroundImage: "/images/heroBackground5.png",
            title: 'SiBa Food',
            description: `Được ra đời với mục đích phục vụ những nhu cầu thiết yếu về bữa ăn hàng ngày của mọi gia đình. Tại SIBA FOOD , chúng tôi luôn đặt tiêu chí phục vụ <b>“TƯƠI NGON – AN TOÀN – TIỆN ÍCH”</b> để đem đến sự hài lòng nhất cho khách hàng.`,
            data1: {number: '20+', text: 'Khối ngành tuyển dụng'},
            data2: {number: '40+', text: 'Việc làm đang tuyển'},
            data3: {number: '2.000+', text: 'Ứng viên ứng tuyển'}, // data 1 2 3 dùng cho layout hero1
          }
        },
        // {
        //     component: 'hero',
        //     display: 'hero3',
        //     options: {
        //         image:"/images/heroImage3.png",
        //         data: [
        //             {
        //                 "title": "Tầm nhìn",
        //                 "text": "Khát vọng hướng tới của Tân Long Group là trở thành một Tập đoàn hàng đầu Việt Nam trong lĩnh vực Nông nghiệp xanh, sạch và phát triển bền vững"
        //             },
        //             {
        //                 "title": "Sứ mệnh",
        //                 "text": "Mở rộng kết nối và gia tăng chuỗi giá trị bền vững của các sản phẩm nông nghiệp bởi công nghệ hiện đại và các giải pháp tối ưu nhằm đáp ứng mọi mong đợi của người tiêu dùng trong nước và quốc tế, góp phần nâng cao thương hiệu quốc gia về nông nghiệp"
        //             },
        //             {
        //                 "title": "Mục tiêu",
        //                 "text": "Tân Long Group luôn nỗ lực vì niềm tin của cộng đồng về một thương hiệu nông nghiệp quốc gia mà Tân Long Group góp phần tạo dựng nên. Tân Long Group liên tục cải tiến chất lượng sản phẩm, chất lượng là yếu tố tạo nên sự phát triển bền vững"
        //             }
        //         ]
        //     }
        // },
        // {
        //     component: 'search',
        //     display: 'search1',
        //     options: {}
        // },
        // {
        //     component: 'carousel',
        //     display: 'carousel1',
        //     options: {
        //         title: 'Công việc nổi bật',
        //         data: 'recruitments',
        //         query: 's={"tags":{"$in":"hot-job"}}',
        //         child: 'type1',
        //         jsonata: `$map($, function($v) {
        //             {
        //                 "id": ($v.id),
        //                 "name": ($v.name),
        //                 "location": ($v.location),
        //                 "image": ($v.image),
        //                 "description": ($v.description),
        //                 "hot_job": ($v.hot_job),
        //                 "salary": ($v.maxSalary) > 0 ? ($v.maxSalary) : 'Thoả thuận',
        //                 "expiration_date": ($v.expiration_date) != null ? $toMillis($v.expiration_date) ~> $fromMillis('[D]/[M]/[Y]') : 'Không giới hạn thời gian'
        //             }
        //         })`
        //     }
        // },
        // {
        //     component: 'gallery',
        //     display: 'gallery2',
        //     options: {
        //         title: 'Tin tức',
        //         data: '/articles',
        //         child: 'type2',
        //         jsonata: `
        //         $map($, function($v) {
        //             {
        //                 "id": ($v.id),
        //                 "name": ($v.title),
        //                 "subtitle": ($v.subtitle),
        //                 "excerpt": ($v.excerpt),
        //                 "image": ($v.image),
        //                 "markdown": ($v.markdown),
        //                 "date": ($v.ts)
        //             }
        //         })
        //         `
        //     }
        // },
        {
          component: 'carousel',
          display: 'carousel2',
          options: {
            title: 'Hot Jobs',
            subtitle: 'Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry\'s standard dummy text ever since the 1500s,',
            data: 'recruitments',
            query: 's={"tags":{"$in":"hot-job"}}',
            child: 'type1',
            jsonata: `$map($, function($v) {
                            {
                                "id": ($v.id),
                                "name": ($v.name),
                                "location": ($v.location),
                                "image": ($v.image),
                                "description": ($v.description),
                                "hot_job": ($v.hot_job),
                                "salary": ($v.maxSalary) > 0 ? ($v.maxSalary) : 'Thoả thuận',
                                "expiration_date": ($v.expiration_date) != null ? $toMillis($v.expiration_date) ~> $fromMillis('[D]/[M]/[Y]') : 'Không giới hạn thời gian'
                            }
                        })`
          }
        },
        {
          component: 'hero',
          display: 'hero6',
          options: {
            title: 'Khối ngành văn phòng',
            image: '/images/homePic1.png',
            description: 'Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry\'s standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book. It has survived not only five centuries, but also the leap into electronic typesetting, remaining essentially unchanged. It was popularised in the 1960s with the release of Letraset sheets containing Lorem Ipsum passages, and more recently with desktop publishing software like Aldus PageMaker including versions of Lorem Ipsum',
            isImageLeft: false
          }
        },

        {
          component: 'hero',
          display: 'hero6',
          options: {
            title: 'Khối ngành nhà máy',
            image: '/images/homePic2.png',
            description: 'Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry\'s standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book. It has survived not only five centuries, but also the leap into electronic typesetting, remaining essentially unchanged. It was popularised in the 1960s with the release of Letraset sheets containing Lorem Ipsum passages, and more recently with desktop publishing software like Aldus PageMaker including versions of Lorem Ipsum',
            isImageLeft: true
          }
        },
        {
          component: 'subscribe',
          display: 'subscribe2',
          options: {
            title: 'Đăng ký nhận tin',
            placeholder: 'Nhập email của bạn',
            btnTitle: 'Gửi'
          }
        }
      ]
    },
    {
      name: 'Trang Recruitments',
      title: 'Công ty Hệ thống Thông tin FPT',
      description: 'FPT IS mong muốn trở thành công ty cung cấp các giải pháp phần mềm và dịch vụ công nghệ thông tin toàn cầu, luôn luôn sáng tạo các giá trị vì khách hàng, đem lại cuộc sống hạnh phúc cho toàn thể các thành viên, đóng góp cho cộng đồng.',
      thumbnail: '',
      href: '/recruitments',
      body: [
        {
          component: 'switcher',
          display: 'switcher1',
          options: {
            componentDefault: {
              component: 'carousel',
              display: 'carousel5',
              options: {
                title: 'Công việc Hot',
                subtitle: 'All the Lorem Ipsum generators on the Internet tend to repeated hehehe. All the Lorem Ipsum generators on the Internet tend to repeated hehehe.',
                data: 'recruitments',
                query: 's={"tags":{"$in":"hot-job"}}',
                child: 'type1',
                jsonata: `
                    $map($, function($v) {
                      {
                          "id": ($v.id),
                          "name": ($v.name),
                          "location": ($v.location),
                          "image": ($v.image),
                          "description": ($v.description),
                          "hot_job": ($v.hot_job),
                          "salary": ($v.maxSalary) > 0 ? ($v.maxSalary) : 'Thoả thuận',
                          "expiration_date": ($v.expiration_date) != null ? $toMillis($v.expiration_date) ~> $fromMillis('[D]/[M]/[Y]') : 'Không giới hạn thời gian'
                      }
                    })`
              }
            },
            componentChange: {
              component: 'list',
              display: 'list6',
              options: {
                data: 'recruitments',
                child: 'type1',
                jsonata: `
                  $map($, function($v) {
                    {
                        "id": ($v.id),
                        "name": ($v.name),
                        "location": ($v.location),
                        "image": ($v.image),
                        "description": ($v.description),
                        "hot_job": ($v.hot_job),
                        "salary": ($v.maxSalary) > 0 ? ($v.maxSalary) : 'Thoả thuận',
                        "expiration_date": ($v.expiration_date) != null ? $toMillis($v.expiration_date) ~> $fromMillis('[D]/[M]/[Y]') : 'Không giới hạn thời gian'
                    }
                  })
                 `
              },
            },
            componentController: {
              component: 'search',
              display: 'search2',
              options: {}
            }
          },
        }
      ]
    },
    {
      name: 'Trang tin tức',
      title: 'Công ty Hệ thống Thông tin FPT',
      description: 'FPT IS mong muốn trở thành công ty cung cấp các giải pháp phần mềm và dịch vụ công nghệ thông tin toàn cầu, luôn luôn sáng tạo các giá trị vì khách hàng, đem lại cuộc sống hạnh phúc cho toàn thể các thành viên, đóng góp cho cộng đồng.',
      thumbnail: '',
      href: '/news',
      body: [

        {
          component: 'gallery',
          display: 'gallery2',
          options: {
            title: 'Tin nổi bật',
            data: '/articles',
            child: 'type2',
            jsonata: `
              $map($, function($v) {
                  {
                      "id": ($v.id),
                      "name": ($v.title),
                      "subtitle": ($v.subtitle),
                      "excerpt": ($v.excerpt),
                      "image": ($v.image),
                      "markdown": ($v.markdown),
                      "date": ($v.ts)
                  }
              })
             `
          }
        },

        {
          component: 'list',
          display: 'list7',
          options: {
            title: 'Tin tức và sự kiện',
            data: '/articles',
            child: 'type2',
            jsonata: `
              $map($, function($v) {
                  {
                      "id": ($v.id),
                      "name": ($v.title),
                      "subtitle": ($v.subtitle),
                      "excerpt": ($v.excerpt),
                      "image": ($v.image),
                      "markdown": ($v.markdown),
                      "date": ($v.ts)
                  }
              })
             `
          }
        },
      ]
    },
    {
      name: 'Trang tra cứu',
      title: 'Công ty Hệ thống Thông tin FPT',
      description: 'FPT IS mong muốn trở thành công ty cung cấp các giải pháp phần mềm và dịch vụ công nghệ thông tin toàn cầu, luôn luôn sáng tạo các giá trị vì khách hàng, đem lại cuộc sống hạnh phúc cho toàn thể các thành viên, đóng góp cho cộng đồng.',
      thumbnail: '',
      href: '/research',
      body: [
        {
          component: 'research',
          display: 'research1',
          options: {
            title: 'Tra cứu ứng viên'
          }
        }
      ]
    },
    {
      name: 'Trang đăng ký',
      title: 'Công ty Hệ thống Thông tin FPT',
      description: 'FPT IS mong muốn trở thành công ty cung cấp các giải pháp phần mềm và dịch vụ công nghệ thông tin toàn cầu, luôn luôn sáng tạo các giá trị vì khách hàng, đem lại cuộc sống hạnh phúc cho toàn thể các thành viên, đóng góp cho cộng đồng.',
      thumbnail: '',
      href: '/apply',
      body: [
        {
          component: 'form',
          display: 'form1',
          options: {
            title: 'Thông tin ứng viên'
          }
        }
      ]
    },
    {
      name: 'Trang đăng ký riêng',
      title: 'Công ty Hệ thống Thông tin FPT',
      description: 'FPT IS mong muốn trở thành công ty cung cấp các giải pháp phần mềm và dịch vụ công nghệ thông tin toàn cầu, luôn luôn sáng tạo các giá trị vì khách hàng, đem lại cuộc sống hạnh phúc cho toàn thể các thành viên, đóng góp cho cộng đồng.',
      thumbnail: '',
      href: '/apply-private',
      body: [
        {
          component: 'form-private',
          display: 'form1',
          options: {
            title: 'Thông tin ứng viên'
          }
        }
      ]
    },
    {
      name: 'Trang giới thiệu',
      title: 'Công ty Hệ thống Thông tin FPT',
      description: 'FPT IS mong muốn trở thành công ty cung cấp các giải pháp phần mềm và dịch vụ công nghệ thông tin toàn cầu, luôn luôn sáng tạo các giá trị vì khách hàng, đem lại cuộc sống hạnh phúc cho toàn thể các thành viên, đóng góp cho cộng đồng.',
      thumbnail: '',
      href: '/introduction',
      body: [
        {
          component: 'list',
          display: 'list4',
          options: {
            title: "A Lot of Features",
            dataList: [
              {
                name: "Feature 1",
                description: "All the Lorem Ipsum generators on the Internet tend to repeated predefined chunks as necessary",
                icon: "/images/testFeatureIcon1.svg"
              },
              {
                name: "Feature 2",
                description: "All the Lorem Ipsum generators on the Internet tend to repeated predefined chunks as necessary",
                icon: "/images/testFeatureIcon2.svg"
              },
              {
                name: "Feature 3",
                description: "All the Lorem Ipsum generators on the Internet tend to repeated predefined chunks as necessary",
                icon: "/images/testFeatureIcon3.svg"
              },
              {
                name: "Feature 4",
                description: "All the Lorem Ipsum generators on the Internet tend to repeated predefined chunks as necessary",
                icon: "/images/testFeatureIcon1.svg"
              },
              {
                name: "Feature 5",
                description: "All the Lorem Ipsum generators on the Internet tend to repeated predefined chunks as necessary",
                icon: "/images/testFeatureIcon2.svg"
              },
              {
                name: "Feature 6",
                description: "All the Lorem Ipsum generators on the Internet tend to repeated predefined chunks as necessary",
                icon: "/images/testFeatureIcon3.svg"
              },
            ]
          },


        },
        {
          component: 'gallery',
          display: 'gallery3',
          options: {
            title: 'Một số hình ảnh hoạt động của Shiba Food',
            data: [
              {
                image: '/images/introductionActivityPic2.png',
              },
              {
                image: '/images/introductionActivityPic2.png',
              },
              {
                image: '/images/introductionActivityPic2.png',
              },
              {
                image: '/images/introductionActivityPic2.png',
              },
              {
                image: '/images/introductionActivityPic2.png',
              },
              {
                image: '/images/introductionActivityPic6.png',
              },

            ]
          },


        },
        {
          component: 'list',
          display: 'list5',
          options: {
            title: 'Giới thiệu chung',
            dataList: [
              {
                title: 'Tầm nhìn',
                description: `Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry\'s standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book. It has survived not only five centuries, but also the leap into electronic typesetting, remaining essentially unchanged. It was popularised in the 1960s with the release of Letraset sheets containing Lorem Ipsum passages, and more recently with desktop publishing software like Aldus PageMaker including versions of Lorem Ipsum.`
              },
              {
                title: 'Sứ mệnh',
                description: `Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry\'s standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book. It has survived not only five centuries, but also the leap into electronic typesetting, remaining essentially unchanged. It was popularised in the 1960s with the release of Letraset sheets containing Lorem Ipsum passages, and more recently with desktop publishing software like Aldus PageMaker including versions of Lorem Ipsum.`
              },
              {
                title: 'Văn hóa doanh nghiệp',
                description: `Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry\'s standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book. It has survived not only five centuries, but also the leap into electronic typesetting, remaining essentially unchanged. It was popularised in the 1960s with the release of Letraset sheets containing Lorem Ipsum passages, and more recently with desktop publishing software like Aldus PageMaker including versions of Lorem Ipsum.`
              },
              {
                title: 'Cơ hội phát triển',
                description: `Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry\'s standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book. It has survived not only five centuries, but also the leap into electronic typesetting, remaining essentially unchanged. It was popularised in the 1960s with the release of Letraset sheets containing Lorem Ipsum passages, and more recently with desktop publishing software like Aldus PageMaker including versions of Lorem Ipsum.`
              }
            ],
          },
        }
      ]
    },
  ],
  footers: {
    display: 'footer1',
    logo: logoLink,
    data: [
      {
        childName: 'Hệ thống cửa hàng miền Bắc',
        childData: [ 'Quận Hà Đông', 'Quận Nam Từ Liêm', 'Quận Bắc Từ Liêm', 'Quận Đống Đa'],
      },
      {
        childName: 'Hệ thống cửa hàng miền Nam',
        childData: [ 'Quận 10', 'Quận Tân Phú', 'Quận Gò Vấp', 'Quận Bình Thạnh'],
      },
    ],
    description: 'Hành trình của bạn tại FPT Information System bắt đầu từ đây'
  }
}
let hostname = '';
const api = () => {
  return process.env.NEXT_PUBLIC_API_URL ? process.env.NEXT_PUBLIC_API_URL : `https://${ hostname }/api`
}

export function setHostname(value: string) {
  hostname = value;
}

export function getLayout(href: string) {
  const page = layout.pages.find(e => e.href == href);
  if (page != undefined) {
    const convertColor = convert.hex.hsl(layout.config.primaryColor);
    return {
      config: {
        ...layout.config,
        hoverColor: `hsl(${ convertColor[0] }, ${ convertColor[1] }%, ${ convertColor[2] + 10 }%)`,
        focusColor: `hsl(${ convertColor[0] }, ${ convertColor[1] }%, ${ convertColor[2] - 10 }%)`
      },
      headers: layout.headers,
      pages: page,
      footers: layout.footers
    }
  } else {
    return layout
  }
}

// Get data dynamic collection
// * url là hostname truyền vào: khi call func từ useEffect thì không lấy được hostname set ở getServerProps nên phải truyền thêm hostname qua props hoặc router
// * collection chỉ định lấy data từ đâu, riêng trường hợp recruitment ở ehiring phải xử lý riêng vì cần mapping location từ masterdata
// * query dynamic
// * jsonata để format lại data theo mẫu, được nhận từ file config
export async function getDataFromCollection(url: string, collection: string, query: string, _jsonata: string) {
  if (collection === '/articles') {
    let data;
    if (query !== '') {
      data = await axios.get(`https://icity.paas.ttgt.vn/ttgt-tphcm/articles/${ query }`);
    } else {
      data = await axios.get('https://icity.paas.ttgt.vn/ttgt-tphcm/articles');
    }

    const expression = jsonata(_jsonata);
    return expression.evaluate(data.data);
  } else {
    const listData = await axios.get(`${ process.env.NEXT_PUBLIC_API_URL ? process.env.NEXT_PUBLIC_API_URL : url }/portals/${ collection }?${ query }`);
    const expression = jsonata(_jsonata);
    const dataFormated = Array.isArray(listData.data.data) ? expression.evaluate(listData.data.data) : expression.evaluate(listData.data);
    if (collection == 'recruitments') {
      const masterdata = await getMasterdata(process.env.NEXT_PUBLIC_API_URL ? process.env.NEXT_PUBLIC_API_URL : url, 'site');
      if (masterdata != null || masterdata != undefined) {
        // Lặp lại mảng data get được để lấy field location, mapping với masterdata trả về data mới đã mapping location
        const arrLocation = listData.data.data.map((dt: any) => {
          const arrStr = dt.location.split(';');
          const mapping = arrStr.map((location: string) => {
            const value = masterdata.find((site: { value: string, label: string }) => site.value == location);
            return value?.label || '';
          });
          dt.location = dt.location != '' ? mapping.join(', ') : 'Không có thông tin';
          return dt;
        });
        // ======================
        return Array.isArray(arrLocation) ? expression.evaluate(arrLocation) : expression.evaluate(arrLocation)
      } else {
        return dataFormated
      }
    } else return dataFormated
  }
}

export async function uploadFile(file: File) {
  let bodyFormData = new FormData();
  bodyFormData.append('file', file);
  return axios.post('https://cf66-2a09-bac5-d41d-18be-00-277-4b.ap.ngrok.io/api/portals/file-manager', bodyFormData, {
    headers: {
      'Content-Type': 'multipart/form-data',
      'Access-Control-Allow-Origin': '*'
    }
  });
}

export async function postTalent(url: string, talent: any, _file: File) {
  if (_file) {
    let fileRes;
    fileRes = await uploadFile(_file);
    console.log('ID file ', fileRes)
    talent.cv.push(fileRes.data._id);
    console.log('Talent ', talent);
    return axios.post(`${ process.env.NEXT_PUBLIC_API_URL ? process.env.NEXT_PUBLIC_API_URL : url }/portals/talents`, talent).catch(err => err.message);
  } else {
    return axios.post(`${ process.env.NEXT_PUBLIC_API_URL ? process.env.NEXT_PUBLIC_API_URL : url }/portals/talents`, talent).catch(err => err.message);
  }
}

export async function getMasterdata(url: string, masterdata: string) {
  const res = await axios.get(`${ process.env.NEXT_PUBLIC_API_URL ? process.env.NEXT_PUBLIC_API_URL : url }/portals/masterdata/${ masterdata }/data`);
  return res.data.data.map((e: { code: string, name: string }) => {
    return {value: e.code, label: e.name}
  });
}

export async function getTalentByCode(url: string, code: string) {
  const data = await axios.get(`${ process.env.NEXT_PUBLIC_API_URL ? process.env.NEXT_PUBLIC_API_URL : url }/portals/applications/${ code }`);
  const masterdataLocation = await getMasterdata(url, 'location');
  const masterdataExaminationMethod = await getMasterdata(url, 'examination-method');
  const masterdataInterviewMethod = await getMasterdata(url, 'interview-method');
  data.data.schedules = mappingArrayObj(data.data.schedules, masterdataLocation, 'location');
  const interviewArr = mappingArrayObj(
    data.data.schedules.filter((cond: { scheduleType: string; }) => cond.scheduleType == 'Interview'),
    masterdataInterviewMethod, 'scheduleMethod');
  const examinationArr = mappingArrayObj(
    data.data.schedules.filter((cond: { scheduleType: string; }) => cond.scheduleType == 'Examination'),
    masterdataExaminationMethod, 'scheduleMethod');
  data.data.schedules = interviewArr.concat(examinationArr);
  data.data.schedules = data.data.schedules.filter((e: any) => e.status == 'pending');

  data.data.recruitmentPlan = {
    name: data.data.recruitmentPlan.name,
    stages: data.data.recruitmentPlan.stages.map((ele: any) => {
      const schedules = data.data.schedules.filter((e: any) => e.scheduleType == ele.type);
      return {...ele, schedules};
    })
  }

  return data.data;
}

// arr1 là mảng data cần mapping
// arr2 là list các phần tử để so sánh mapping
function mappingArrayObj(arr1: Array<any>, arr2: Array<any>, keyMapping: string) {
  return arr1.map((e, i) => {
    let temp = arr2.find(element => element.value === e[keyMapping]);
    if (temp) {
      e[keyMapping] = temp.label;
    }
    return e;
  })
}
