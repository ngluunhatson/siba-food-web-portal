import { Center, Group, Image, Text } from "@mantine/core";
import Link from 'next/link';
import { useRouter } from 'next/router';
import styles from "styles/menu.module.scss";
import { slide as Burger } from 'react-burger-menu'
import variables from 'styles/variables.module.scss';
import { useState } from "react";

export default function Menu(props) {

  let eleHTML = <></>;
  switch (props.display) {
    case "basic":

      if (props.windowDimension.width >= 750)
        eleHTML = <BasicMenu { ...props } windowDimension={ props.windowDimension }/>
      else
        eleHTML = <SmallMenu { ...props } windowDimension={ props.windowDimension }/>

      break;
    default:
      break;
  }
  return eleHTML;
}

function BasicMenu(props) {
  const router = useRouter();
  const divider = (props.windowDimension.width < 1440) ? (1440 / props.windowDimension.width) : 1;
  const padding = 64 / divider

  const eleHTML = (
    <Group
      spacing={ 108 / divider }
      noWrap
      style={ {height: 96 / divider, background: "#FFF", padding: `0 ${ padding }px`} }
      align="center"
      position={ "apart" }
      className={ styles.menu1 }
    >
      <Image src={ props.logo } width={ 200 / divider } alt="logo demo"/>
      <Group spacing={ 32 / divider }>
        { props.data.map((ele, index) => (
          <Center className={ styles.boxText } key={ `menu-${ index }` }>
            <Link href={ ele.href }>
              <Text fz={ 18 / (divider) }
                    className={ `${ styles.text } ${ router.asPath == ele.href ? styles.active : '' }` }>
                { ele.title }
              </Text>
            </Link>
          </Center>
        )) }
      </Group>
      <Group align='center' spacing={ 37 / divider }>

        <a target={ "_blank" } rel="noreferrer"
           href={ props.twitter }><Image src="/images/fa-twitter.svg"
                                         width={ 18 } alt={ '' }/></a>
        <a target={ "_blank" } rel="noreferrer"
           href={ props.facebook }><Image src="/images/fa-facebook-square.svg"
                                          width={ 18 }
                                          alt={ '' }/></a>
        <a target={ "_blank" } rel="noreferrer"
           href={ props.googlePlus }><Image src="/images/fa-google-plus.svg"
                                            width={ 18 }
                                            alt={ '' }/></a>

      </Group>
    </Group>
  );
  return eleHTML;
}

function SmallMenu(props) {
  const router = useRouter();
  const divider = (750 / props.windowDimension.width)
  const burgerStyles = {
    bmBurgerButton: {
      position: 'fixed',
      width: 36 / divider,
      height: 30 / divider,
      right: 36 / divider,
      top: 20 / divider
    },
    bmBurgerBars: {
      background: variables.primaryColor
    },
    bmBurgerBarsHover: {
      background: variables.hoverColor
    },
    bmCrossButton: {
      height: '24px',
      width: '24px'
    },
    bmCross: {
      background: variables.secondColor
    },
    bmMenuWrap: {
      position: 'fixed',
      height: '100%',
      right: '0px',
      top: '0px',
      width: '100%',
    },
    bmMenu: {
      background: '#FFFFF2',

      borderLeft: `1px solid ${ variables.primaryColor }`,
      padding: '2.5em 1.5em 0',
      fontSize: '1.15em'
    },
    bmMorphShape: {},
    bmItemList: {},
    bmItem: {
      marginBottom: 10 / divider
    },
    bmOverlay: {}

  }
  const [burgerOpen, setBurgerOpen] = useState(false);

  const eleHTMl = (
      <Group
        noWrap
        style={ {
          height: 96 / divider,
          background: "#FFF",
          padding: '0px ' + 32 / divider + 'px',
          position: 'fixed',
          top: 0,

        } }
        align="center"
        position={ "apart" }
        className={ styles.menuSmall }
      >
        <Image src={ props.logo } width={ 150 / divider } alt="logo demo"/>

        <div onClick={ () => setBurgerOpen(true) }>
          <Burger isOpen={ burgerOpen } styles={ burgerStyles }>
            { props.data.map((ele, index) => (
              <Link className={ styles.boxText }
                    href={ ele.href }
                    onClick={ (e) => {
                      e.stopPropagation();
                      setBurgerOpen(false);
                    } }
                    key={ `menu-${ index }` }>
                < Text fz={ 16 } c={ 'black' }
                       className={ `${ styles.text } ${ router.asPath === ele.href ? styles.active : '' }` }>
                  { ele.title }
                </Text>
              </Link>
            )) }
            <a target={ "_blank" } rel="noreferrer"
               href={ props.twitter }><Image src="/images/fa-twitter.svg"
                                             width={ 16 } alt={ '' }/></a>


            <a target={ "_blank" } rel="noreferrer"
               href={ props.facebook }><Image src="/images/fa-facebook-square.svg"
                                              width={ 16 }
                                              alt={ '' }/></a>


            <a target={ "_blank" } rel="noreferrer"
               href={ props.googlePlus }><Image src="/images/fa-google-plus.svg"
                                                width={ 16 }
                                                alt={ '' }/></a>

          </Burger>
        </div>
      </Group>
    )
  ;
  return eleHTMl;
}

