import { Fab } from "react-tiny-fab";
import { Image } from "@mantine/core";
import { useEffect, useState } from "react";

export default function FabButton(props) {
  const [domLoaded, setDomLoaded] = useState(false);

  useEffect(() => {
    setDomLoaded(true);
  }, []);
  let eleHTML = <></>;
  if (domLoaded)
    switch (props.display) {
      case'fab1':
        eleHTML = <FabButton1 backgroundColor={ props.options.backgroundColor } href={ props.options.href }
                              icon={ props.options.icon }/>
        break;
      default:
        break;
    }

  return eleHTML;
}

function FabButton1(props) {
  const [mouseHover, setMouseHover] = useState(false);

  const handleMouseEnter = () => {
    setMouseHover(true);
  }

  const handleMouseLeave = () => {
    setMouseHover(false);
  }

  const defaultSizeButton = 56;
  const defaultSizeIcon = 24;
  const multiplier = 1.1;

  const eleHTML = (
    <Fab onMouseEnter={ handleMouseEnter } onMouseLeave={ handleMouseLeave } mainButtonStyles={ {
      backgroundColor: props.backgroundColor,
      width: mouseHover ? defaultSizeButton * multiplier : defaultSizeButton,
      height: mouseHover ? defaultSizeButton * multiplier : defaultSizeButton,
      position: 'fixed',
      right: 16,
      bottom: 16,
    } }
         icon={
           <a href={ props.href } target={ "_blank" } rel={ "noreferrer" }>
             <Image alt="fab-icon" width={ mouseHover ? defaultSizeIcon * multiplier : defaultSizeIcon }
                    src={ props.icon }/>
           </a>
         }
         event={ '' }
    >
    </Fab>
  );
  return eleHTML;
}
