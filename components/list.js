/* eslint-disable react-hooks/exhaustive-deps */
import {
  Box,
  Button,
  Card,
  Center,
  Divider,
  Grid,
  Group,
  Image,
  Loader,
  ScrollArea,
  Select,
  SimpleGrid,
  Stack,
  Text,
  TextInput,
} from "@mantine/core";
import { At, ChevronLeft, Clock, CurrencyDollar, Location, Tag } from "tabler-icons-react";
import styles from "../styles/feature.module.scss";
import React, { useEffect, useState } from "react";
import { getDataFromCollection, getMasterdata } from "@/services/services";
import variables from "/styles/variables.module.scss";
import Link from "next/link";
import { useRouter } from "next/router";
import Action from "./action";
import { openModal } from "@mantine/modals";
import parse from "html-react-parser";
import dayjs from "dayjs";
import md from "markdown";

export default function List(props) {
  let eleHTML = <></>;
  switch (props.display) {
    // list1 - dạng stack + filter bên phải
    case "list1":
      eleHTML = <List1 url={ props.url } optionData={ props.options.data } optionQuery={ "" }
                       optionTypeChild={ props.options.child }
                       optionJsonata={ props.options.jsonata } title={ props.options.title }/>
      break;

    //list2 - dạng 1 page, list data bên trái và hiển thị detail bên phải
    case "list2":
      eleHTML = <List2 url={ props.url } optionData={ props.options.data } optionQuery={ "" }
                       optionTypeChild={ props.options.child }
                       optionJsonata={ props.options.jsonata }/>
      break;

    //list3 - list dạng stack với image ~ list articles
    case "list3":
      eleHTML = <List3 tilte={ props.options.title }/>;
      break;

    //list4 - Feature list in /introduction
    case "list4":
      eleHTML = <List4R { ...props.options } background={ props.backgroundLightGrey }
                        windowDimension={ props.windowDimension }/>;
      break;

    //List5 - List cho giá trị cốt lõi trong /introduction
    case "list5":
      eleHTML = <List5R { ...props.options } windowDimension={ props.windowDimension }/>;
      break;

    //List6 - list2 version2
    case "list6":
      eleHTML = <List6R url={ props.url } optionData={ props.options.data } optionQuery={ "" }
                        optionTypeChild={ props.options.child }
                        optionJsonata={ props.options.jsonata } windowDimension={ props.windowDimension }/>

      break;

    //List7- list cho /news
    case "list7":
      eleHTML = <List7R url={ props.url } optionData={ props.options.data } optionQuery={ "" }
                        optionTypeChild={ props.options.child } optionTitle={ props.options.title }
                        optionJsonata={ props.options.jsonata } windowDimension={ props.windowDimension }/>

      break;

    default:
      break;
  }
  return eleHTML;
}


function loadData(loadOptions, setDataFunc, setFilterDataFunc, setDetailFunc, setSiteFunc) {
  const loadDataFunc = async () => await getDataFromCollection(
    loadOptions.url, loadOptions.data, loadOptions.query, loadOptions.jsonata
  );

  const loadSiteFunc = async () => await getMasterdata(loadOptions.url, "site");

  loadDataFunc().then(res => {
    setDataFunc(res);
    setFilterDataFunc(res);
    setDetailFunc(res[0]);
  });

  loadSiteFunc().then(sites => {
    setSiteFunc(sites);
  });
}

function openFormApply(ele) {
  openModal({
    centered: true,
    title: (
      <Text fz={ 25 } fw={ 500 } c={ variables.primaryColor }>
        Ứng tuyển { ele.name }
      </Text>
    ),
    children: <Action display="apply-short" recruitmentPlan={ ele.id }/>,
  });
}

function List1(props) {
  const [data, setData] = useState([]);
  const [filterData, setFilterData] = useState([]);
  const [site, setSite] = useState(); // data cho filter select theo vị trí
  const router = useRouter();

  useEffect(() => {
    loadData({
        url: props.url,
        data: props.optionData,
        query: props.optionQuery,
        jsonata: props.optionJsonata
      },
      setData, setFilterData, () => {
      }, setSite);
  }, []);
  const eleHTML = (
    <Box
      className={ styles.list1 }
      sx={ () => ({
        padding: "0px 64px",
      }) }
    >
      <Stack mb={ 32 }>
        <Text
          className={ styles.title }
        >{ `${ props.title } (${ data.length })` }</Text>
        <div className={ styles.boxDivider }>
          <Divider size={ 1 } color={ variables.primaryColor }/>
        </div>
      </Stack>
      <Grid gutter={ 32 }>
        <Grid.Col pt={ 0 } span={ 8 } className={ styles.scroll }>
          <ScrollArea style={ {height: "55vh"} } type="never">
            { filterData.map((ele, index) => (
              <Card
                key={ `list-${ index }` }
                p={ 36 }
                radius={ 32 }
                shadow="sm"
                withBorder
                mb={ 32 }
              >
                <Group align="center" position="apart">
                  <Stack>
                    <Group>
                      <Clock
                        size={ 24 }
                        strokeWidth={ 1.5 }
                        color={ "rgba(37, 37, 37, 0.7)" }
                      />
                      <Text>{ ele.expiration_date }</Text>
                    </Group>
                    <Link
                      href={ `/${ router.query.id }/${ ele.id }?type_child=${ props.optionTypeChild }` }
                      style={ {cursor: "pointer"} }
                    >
                      <Text className={ styles.jobName } lineClamp={ 1 }>
                        { ele.name }
                      </Text>
                    </Link>
                    <Group spacing={ 32 }>
                      <Group>
                        <Location
                          size={ 24 }
                          strokeWidth={ 1.5 }
                          color={ "rgba(37, 37, 37, 0.7)" }
                        />
                        <Text>{ ele.location }</Text>
                      </Group>
                      <Group>
                        <CurrencyDollar
                          size={ 24 }
                          strokeWidth={ 1.5 }
                          color={ "rgba(37, 37, 37, 0.7)" }
                        />
                        <Text>{ ele.salary }</Text>
                      </Group>
                    </Group>
                  </Stack>
                  <Button
                    className={ styles.btnApply }
                    size="lg"
                    onClick={ () => {
                      openFormApply(ele);
                    } }
                  >
                    Ứng tuyển
                  </Button>
                </Group>
              </Card>
            )) }
          </ScrollArea>
        </Grid.Col>
        <Grid.Col span={ 4 } style={ {background: "#FFF", borderRadius: 32} }>
          <Card radius={ 32 } p={ 36 }>
            <Stack mb={ 16 }>
              <Text className={ styles.subtitle }>
                Chọn lọc theo tiêu chí
              </Text>
              <div className={ styles.boxDivider }>
                <Divider size={ 1 } color={ variables.primaryColor }/>
              </div>
            </Stack>
            <Stack>
              <TextInput
                radius="md"
                placeholder="Bạn đang tìm công việc gì?"
                label="Tên công việc"
                onChange={ (e) => {
                  setFilterData(
                    data.filter((ele) => ele.name.includes(e.target.value))
                  );
                } }
              />
              <Select
                radius="md"
                placeholder="Bạn muốn làm việc tại đâu"
                label="Nơi làm việc"
                data={
                  site ? [...site, {value: "all", label: "Tất cả"}] : []
                }
                onChange={ (e) => {
                  if (e == "all") {
                    setFilterData(data);
                  } else {
                    const siteSelect = site.find((site) => site.value == e);
                    // const arLocation = data
                    setFilterData(
                      data.filter((ele) => {
                        ele.location == siteSelect.label;
                        const splitLocation = ele.location.split(",");
                        console.log(splitLocation);
                        const findLocation = splitLocation.find(
                          (location) => location.trim() == siteSelect.label
                        );
                        console.log(findLocation);
                        return findLocation ? ele : null;
                      })
                    );
                  }
                } }
              />
            </Stack>
          </Card>
        </Grid.Col>
      </Grid>
    </Box>
  );
  return eleHTML;
}

function List2(props) {
  const [filterData, setFilterData] = useState([]);
  const [detail, setDetail] = useState(); // dùng lưu 1 data khi click dùng tại list2

  useEffect(() => {
    loadData({
        url: props.url,
        data: props.optionData,
        query: props.optionQuery,
        jsonata: props.optionJsonata
      },
      () => {
      }, setFilterData, setDetail, () => {
      });
  }, []);


  const eleHTML = (
    <Box
      className={ styles.list2 }
      sx={ {
        padding: "56px 64px 0 64px",
        minHeight: "calc(100vh - 250px - 96px)",
      } }
    >
      <Group spacing={ 64 } noWrap align="flex-start">
        <div className={ styles.list }>
          { filterData.map((ele, index) => (
            <Card
              shadow="md"
              radius={ 16 }
              p={ 32 }
              mb={ 32 }
              key={ `list2-${ index }` }
              onClick={ () => setDetail(ele) }
            >
              <Stack>
                <Text lineClamp={ 1 } className={ styles.title }>
                  { ele.name }
                </Text>
                <Group>
                  <Group>
                    <Clock
                      size={ 24 }
                      strokeWidth={ 1.5 }
                      color={ "rgba(37, 37, 37, 0.7)" }
                    />
                    <Text>{ ele.expiration_date }</Text>
                  </Group>
                  <Group>
                    <Location
                      size={ 24 }
                      strokeWidth={ 1.5 }
                      color={ "rgba(37, 37, 37, 0.7)" }
                    />
                    <Text>{ ele.locationLabel }</Text>
                  </Group>
                  <Group>
                    <CurrencyDollar
                      size={ 24 }
                      strokeWidth={ 1.5 }
                      color={ "rgba(37, 37, 37, 0.7)" }
                    />
                    <Text>{ ele.salary }</Text>
                  </Group>
                </Group>
              </Stack>
            </Card>
          )) }
        </div>
        <div className={ styles.detail }>
          <Group
            position="apart"
            style={ {
              background: `${ variables.primaryColor }33`,
              padding: "32px 64px",
            } }
          >
            <Stack>
              <Text className={ styles.title }>{ detail?.name }</Text>
              <Group>
                <Group>
                  <Clock
                    size={ 24 }
                    strokeWidth={ 1.5 }
                    color={ "rgba(37, 37, 37, 0.7)" }
                  />
                  <Text>{ detail?.expiration_date }</Text>
                </Group>
                <Group>
                  <Location
                    size={ 24 }
                    strokeWidth={ 1.5 }
                    color={ "rgba(37, 37, 37, 0.7)" }
                  />
                  <Text>{ detail?.location }</Text>
                </Group>
                <Group>
                  <CurrencyDollar
                    size={ 24 }
                    strokeWidth={ 1.5 }
                    color={ "rgba(37, 37, 37, 0.7)" }
                  />
                  <Text>{ detail?.salary }</Text>
                </Group>
              </Group>
            </Stack>
            <Button
              className={ styles.btnApply }
              onClick={ () => {
                openFormApply(detail);
              } }
            >
              Ứng tuyển
            </Button>
          </Group>
          <Stack spacing={ 24 } py={ 32 } px={ 64 } bg="white">
            { detail?.description.map((ele, index) => (
              <div key={ `description-${ index }` }>
                <Text className={ styles.subtitle } fz={ 20 } fw={ 500 } mb={ 16 }>
                  { ele.name }
                </Text>
                <Text lh={ "32px" }>{ parse(ele.content) }</Text>
              </div>
            )) }
          </Stack>
        </div>
      </Group>
    </Box>
  );
  return eleHTML;
}

function List3(props) {
  const eleHTML = (
    <Box className={ styles.list3 }>
      <Stack mb={ 32 }>
        <Text className={ styles.title }>{ props.title }</Text>
        <div className={ styles.boxDivider }>
          <Divider size={ 1 } color={ variables.primaryColor }/>
        </div>
      </Stack>
      <Stack spacing={ 44 }>
        <Group spacing={ 24 } noWrap align='flex-start'>
          <Image
            src="https://images.unsplash.com/photo-1676044980738-2872277ba68c?ixlib=rb-4.0.3&ixid=MnwxMjA3fDB8MHx0b3BpYy1mZWVkfDE1fGhtZW52UWhVbXhNfHxlbnwwfHx8fA%3D%3D&auto=format&fit=crop&w=800&q=60"
            alt=""
            withPlaceholder
            placeholder={ <Image src={ props.image } alt=""/> }
            width={ 477 }
            height={ 344 }
            radius={ 32 }
          />
          <Stack spacing={ 0 } className={ styles.containerContent }>
            <Text lineClamp={ 2 } className={ styles.name }>
              Mew mew mewww
            </Text>
            <Text lineClamp={ 4 } className={ styles.summary }>
              {
                "Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat."
              }
            </Text>
            <Group spacing={ 24 } className={ styles.infor }>
              <Group>
                <At
                  size={ 24 }
                  strokeWidth={ 1.5 }
                  color={ "rgba(37, 37, 37, 0.7)" }
                />
                <Text>Admin</Text>
              </Group>
              <Group>
                <Clock
                  size={ 24 }
                  strokeWidth={ 1.5 }
                  color={ "rgba(37, 37, 37, 0.7)" }
                />
                <Text>21/12/2022</Text>
              </Group>
              <Group>
                <Tag
                  size={ 24 }
                  strokeWidth={ 1.5 }
                  color={ "rgba(37, 37, 37, 0.7)" }
                />
                <Text>
                  Default
                </Text>
              </Group>
            </Group>
          </Stack>
        </Group>
      </Stack>
    </Box>
  );
  return eleHTML;
}

function List4R(props) {
  const currentWidth = props.windowDimension.width;
  const divider = Math.min((currentWidth < 1440) ? 1440 / currentWidth : 1, 2.2);
  const dataList = props.dataList.map((dataSingleton, index) =>
    <Stack spacing={16 / divider} key={ `feature-${ index }` }>
      <Group spacing={8 / divider}>
        <Image width={ 24 / divider } alt={ `icon-feature-${ index }` } src={ dataSingleton.icon }/>
        <Text fw={ 700 } fz={ 16 / divider }>{ dataSingleton.name }</Text>
      </Group>
      <Text fz={ 16 / divider } fw={ 400 } color="dimmed" lineClamp={ 3 }>
        { dataSingleton.description }
      </Text>

    </Stack>
  );
  const eleHTML = (
    <Stack style={ {width: "100%", backgroundColor: props.background} } spacing={ 44 / divider }>
      <div></div>
      <Stack spacing={ (currentWidth < 750 ? 20 : 64 / divider) }>
        <Text align="center" fz={ 32 / divider } fw={ 700 }>{ props.title }</Text>
        <Box sx={ {padding: `0px ${ (currentWidth < 750 ? 20 : 144 / divider) }px`} }>
          <SimpleGrid cols={ currentWidth > 1100 && props.dataList.length >= 5 ? 3 : 2 } spacing="xl"
                      verticalSpacing="md">
            { dataList }
          </SimpleGrid>
        </Box>
      </Stack>
      { currentWidth >= 750 ? <div></div> : <></> }
    </Stack>
  );

  return eleHTML;
}


function List5R(props) {
  const currentWidth = props.windowDimension.width;
  const divider = Math.min((currentWidth < 1440) ? 1440 / currentWidth : 1, 1.6);

  const listDataJSX = props.dataList.map((dataSingleton, index) =>
    <Stack spacing={ 16 / divider } key={ `core-value-` + index }>
      <Text fz={ 32 / divider } fw={ 700 }>{ dataSingleton.title }</Text>
      <Text fz={ 16 / divider } color={ "dimmed" }>{ dataSingleton.description }</Text>
    </Stack>
  );

  const eleHTML = (
    <Stack spacing={ 32 / divider } style={ {
      padding: `${ (currentWidth < 750 ? 16 : 44 / divider)  }px ${(currentWidth < 750 ? 20 : 120 / divider)  }px`
    } }>
      <Text ta="left" color={ variables.primaryColor } fz={ 48 / divider } fw={ 700 } c={ variables.primaryColor }>
        { props.title }
      </Text>
      <Divider size={ 1 } style={ {width: "100%"} }
               color={ variables.secondColor }/>
      { listDataJSX }
    </Stack>

  );

  return eleHTML;
}

function List6R(props) {
  const [data, setData] = useState([]);
  const [detail, setDetail] = useState(null);
  const [selectedIndex, setSelectedIndex] = useState(0);
  const currentWidth = props.windowDimension.width;

  useEffect(() => {
    loadData({
        url: props.url,
        data: props.optionData,
        query: props.optionQuery,
        jsonata: props.optionJsonata
      },
      setData, () => {
      }, setDetail, () => {
      });
  }, []);

  const handleCardClick = (cardDetail, index) => {
    setDetail(cardDetail);
    setSelectedIndex(index);
  }

  const listJSX = data.map((dataSingleton, index) => {
    const expirationDate = dataSingleton && dataSingleton.expiration_date ? dayjs(dataSingleton.expiration_date) : "Invalid Date";
    const expirationDateString = expirationDate.toString() === 'Invalid Date' ? 'Không giới hạn' : expirationDate.format('DD/MM/YYYY');
    return (
      <Stack onClick={ () => handleCardClick(dataSingleton, index) }
             key={ `card-${ index }` } className={ styles.card }
             style={ selectedIndex === index
               ? {border: `1px solid ${ variables.primaryColor }`, boxShadow: "0px 0px 30px rgba(0, 136, 72, 0.2)"}
               : null }
             spacing={ 8 }>
        <Group spacing={ 16 }>
          <Text lineClamp={ 1 }
                className={ styles.textTitle }>{ dataSingleton && dataSingleton.name ? dataSingleton.name : 'Chưa Có' }</Text>
          <Group spacing={ 8 }>
            <Image src="/images/tags-filled.svg" width={ 20 } alt={ "" }/>
            <Text fz={ 16 } fw={ 500 } style={ {color: "#007AFF"} }>Nhà máy</Text>
          </Group>
          <div style={ {background: '#F5222D', borderRadius: "8px", width: "44px"} }>
            <Text fz={ 12 } fw={ 400 } ta={ "center" } style={ {padding: "4px", color: 'white'} }>Gấp</Text>
          </div>
        </Group>
        <Group spacing={ 16 }>
          <Group spacing={ 4 }>
            <CurrencyDollar size={ 18 } strokeWidth={ 1.5 }/>
            <Text fz={ 14 }
                  fw={ 400 }>{ dataSingleton && dataSingleton.salary ? dataSingleton.salary : 'Thỏa thuận' }</Text>
          </Group>
          <Group spacing={ 4 }>
            <Image src="/images/location-alt.svg" width={ 16 } alt={ "" }/>
            <Text fz={ 14 }
                  fw={ 400 }>{ dataSingleton && dataSingleton.location ? dataSingleton.location : 'Chưa biết' }</Text>
          </Group>
          <Group spacing={ 4 }>
            <Image src="/images/buildingIcon.svg" width={ 16 } alt={ "" }/>
            <Text fz={ 14 } fw={ 400 }>Tại nhà máy</Text>
          </Group>
          <Group spacing={ 4 }>
            <Image src="/images/clockIcon.svg" width={ 16 } alt={ "" }/>
            <Text fz={ 14 } fw={ 400 }>{ expirationDateString }</Text>
          </Group>
        </Group>
      </Stack>
    );
  });


  let detailJSX = <></>;
  if (detail)
    detailJSX = (
      <>
        <Stack className={ styles.detailHeader } spacing={ 24 }>
          <Stack spacing={ 16 }>
            <Text fz={ (currentWidth >= 750 ? 32 : 24) } fw={ 700 } style={ {color: variables.primaryColor} }>{ detail.name }</Text>
            <Group spacing={ 16 }>
              <Group spacing={ 8 }>
                <CurrencyDollar size={ 22 } strokeWidth={ 1.5 }/>
                <Text className={ styles.textHeader }>{ detail.salary }</Text>
              </Group>
              <Group spacing={ 8 }>
                <Image src="/images/location-alt.svg" width={ 20 } alt={ "" }/>
                <Text className={ styles.textHeader }>{ detail.location }</Text>
              </Group>
              <Group spacing={ 8 }>
                <Image src="/images/buildingIcon.svg" width={ 20 } alt={ "" }/>
                <Text className={ styles.textHeader }>Tại nhà máy</Text>
              </Group>
              <Group spacing={ 8 }>
                <Image src="/images/comment-user.svg" width={ 20 } alt={ "" }/>
                <Text className={ styles.textHeader }>200 members</Text>
              </Group>

              <Group spacing={ 8 }>
                <Image src="/images/clockIcon.svg" width={ 16 } alt={ "" }/>
                <Text className={ styles.textHeader }>{
                  dayjs(detail.expiration_date).toString() === 'Invalid Date'
                    ? ''
                    : dayjs(detail.expiration_date).format('DD/MM/YYYY')
                }</Text>
              </Group>
            </Group>
          </Stack>

          <Group spacing={ 32 }>
            <Button className={ styles.buttonPrimary }>
              <Text className={ styles.textButtonPrimary }>Primary Button</Text>
            </Button>
            <Button className={ styles.buttonSecondary }>
              <Text className={ styles.textButtonSecondary }>Secondary Button</Text>
            </Button>
          </Group>

        </Stack>
        <Stack className={ styles.detailBody } spacing={ 48 }>
          <Stack spacing={ 32 } className={ styles.detailBodyHeader }>
            <Text fz={ 22 } fw={ 700 }>General Info</Text>
            <Stack spacing={ 16 }>
              <Group spacing={ 4 }>
                <Text className={ styles.detailBodyHeaderText }>Salary:</Text>
                <Text className={ styles.detailBodyHeaderText }>{ detail.salary }</Text>
              </Group>
              <Group spacing={ 4 }>
                <Text className={ styles.detailBodyHeaderText }>Field:</Text>
                <Text className={ styles.detailBodyHeaderText }>Technology</Text>
              </Group>
              <Group spacing={ 4 }>
                <Text className={ styles.detailBodyHeaderText }>Level:</Text>
                <Text className={ styles.detailBodyHeaderText }>Junior</Text>
              </Group>
              <Group spacing={ 4 }>
                <Text className={ styles.detailBodyHeaderText }>Address:</Text>
                <Text className={ styles.detailBodyHeaderText }>{ detail.location }</Text>
              </Group>
              <Group spacing={ 4 }>
                <Text className={ styles.detailBodyHeaderText }>Working hour:</Text>
                <Text className={ styles.detailBodyHeaderText }>8 a.m - 5 p.m</Text>
              </Group>
            </Stack>
          </Stack>
          <Stack spacing={ 24 }>
            <Text fz={ 22 } fw={ 700 }>Job Description</Text>
            <Text
              className={ styles.detailBodyDescriptionText }>{ parse(detail.description[0].content) }</Text>
          </Stack>
        </Stack>
      </>
    );

  let eleHTML = <></>;
  if (currentWidth >= 768)
    eleHTML = (
      <Group className={ styles.list6 } spacing={ 32 }>
        <Stack className={ styles.stackList } spacing={ 16 }>
          <Group position={ "apart" }>
            <Text fz={ 16 } fw={ 400 }>{ data.length } Kết quả tìm kiếm</Text>
            <Button className={ styles.sortBtn }>
              <Image src="/images/sortButton.svg" width={ 20 } alt={ "" }/>
            </Button>
          </Group>

          <Stack className={ styles.listCard } spacing={ 16 }>
            { listJSX }
          </Stack>
        </Stack>
        <Stack className={ styles.stackDetail }>
          { detailJSX }
        </Stack>
      </Group>
    );
  else {
    eleHTML = <List6RHelperComponent listJSX={ listJSX }
                                     detailJSX={ detailJSX }
                                     currentWidth={ currentWidth }
                                     dataLength={ data.length }
    />
  }
  return eleHTML;
}

function List6RHelperComponent(props) {
  const [isAtDetailPage, setIsAtDetailPage] = useState(false);

  const handleOnClick = () => {
    setIsAtDetailPage(true);
    document.getElementById("topOfPage").scrollIntoView({behavior: "smooth"});
  }
  const handleOnBack = () => {
    setIsAtDetailPage(false);
  }

  const listContent = (
    <Stack spacing={ 10 } onClick={ handleOnClick }>
      { props.listJSX }
    </Stack>
  );

  const eleHTML = (
    <Stack spacing={ 20 } style={ {padding: 20} } className={ styles.list6 } id="topOfPage">
      <Group position={ 'apart' }>
        { isAtDetailPage ? (<Button style={ {backgroundColor: 'transparent'} } onClick={ handleOnBack }>
          <ChevronLeft color={ 'black' } size={ 25 } strokeWidth={ 2 }
                       style={ {marginLeft: -10 / (768 / props.currentWidth)} }/>
        </Button>) : <div></div> }
        <Group position={ 'center' }>
          <Text fz={ 16 } fw={ 400 }>{ props.dataLength } Kết quả tìm kiếm</Text>
          <Button className={ styles.sortBtn }>
            <Image src="/images/sortButton.svg" width={ 20 } alt={ "" }/>
          </Button>
        </Group>
      </Group>
      <Stack spacing={ 10 }>
        { isAtDetailPage ? props.detailJSX : listContent }
      </Stack>
    </Stack>
  );

  return eleHTML;
}

function List7R(props) {
  const [data, setData] = useState([]);
  const currentWidth = props.windowDimension.width;
  const divider = (currentWidth < 1440) ? 1440 / currentWidth : 1;

  useEffect(() => {
    loadData({
        url: props.url,
        data: props.optionData,
        query: props.optionQuery,
        jsonata: props.optionJsonata
      },
      setData, () => {
      }, () => {
      }, () => {
      });
  }, []);

  const cardList = data.map((dataSingleton, index) =>
    <Link key={ `link-card-${ index }` }
          href={ `${ props.optionData }/${ dataSingleton.id }?type_child=${ props.optionTypeChild }` }>
      <Card shadow="sm" padding="lg" radius="md" withBorder>
        <Card.Section>
          <Image
            src={ dataSingleton.image }
            height={ 200 }
            alt={ dataSingleton.image }
            withPlaceholder
            placeholder={ <Loader color={ variables.primaryColor } variant="bars"/> }
          />
        </Card.Section>

        <Group position="center" mt="md" mb="xs">
          <Text lineClamp={ 1 } weight={ 700 }>{ dataSingleton.excerpt }</Text>

        </Group>

        <Text size="sm" color="dimmed" lineClamp={ 2 }>
          { parse(md.parse(dataSingleton.markdown)) }
        </Text>
      </Card>
    </Link>
  );


  let numPerRow = 3;

  if (currentWidth <= 1000)
    numPerRow = 2;
  if (currentWidth <= 700)
    numPerRow = 1;

  let optionalTitle = <></>;

  if (currentWidth <= 720)
    optionalTitle = (
      <Stack spacing={ 10 }>
        <Text align={ "center" } fz={ 30 } c={ variables.primaryColor }
              fw={ 700 }>{ props.optionTitle }</Text>
        <Center>
          <div style={ {width: '70%'} }>
            <Divider size={ 1 } color={ variables.secondColor }/>
          </div>
        </Center>
      </Stack>
    );
  else
    optionalTitle = (
      <Stack spacing={ 10 }>
        <Text align={ "left" } fz={ 48 } c={ variables.primaryColor }
              fw={ 700 }>{ props.optionTitle }</Text>
        <div style={ {width: '10%'} }>
          <Divider size={ 1 } color={ variables.secondColor }/>
        </div>
      </Stack>
    );

  const eleHTML = (
    <Box className={ styles.list7 } style={ {padding: `${ 44 / divider }px`} }>
      <Stack spacing={ 30 }>
        { optionalTitle }
        <SimpleGrid cols={ numPerRow } spacing="md">
          { cardList }
        </SimpleGrid>
      </Stack>
    </Box>
  );
  return eleHTML;
}
