# Build BASE
FROM node:16-alpine as BASE
LABEL author="huyvn4"

WORKDIR /app
COPY package.json ./
RUN npm i --frozen-lockfile

# Build Image
FROM node:16-alpine AS BUILD
LABEL author="huyvn4"

WORKDIR /app

COPY --from=BASE /app/node_modules ./node_modules
COPY . .
RUN npm run build

# Build production
FROM node:16-alpine AS PRODUCTION
LABEL author="huyvn4"

WORKDIR /app

COPY --from=BUILD /app/package.json ./
COPY --from=BUILD /app/node_modules ./node_modules
COPY --from=BUILD /app/next.config.js ./
COPY --from=BUILD /app/.next/standalone ./
COPY --from=BUILD /app/.next/static ./.next/static
COPY --from=BUILD /app/public ./public
COPY --from=BUILD /app/services ./services
COPY --from=BUILD /app/pages ./pages
COPY --from=BUILD /app/styles ./styles
COPY --from=BUILD /app/components ./components


EXPOSE 3000

CMD ["npm", "start"]
