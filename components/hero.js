import variables from "/styles/variables.module.scss";
import styles from "/styles/feature.module.scss";
import globals from "/styles/globals.module.scss";
import { BackgroundImage, Box, Button, Divider, Group, Image, Stack, Text, } from "@mantine/core";
import parse from "html-react-parser";

export default function Hero(props) {
  let eleHTML = <></>;

  switch (props.display) {
    case "hero1":
      eleHTML = <Hero1 { ...props.options } />;
      break;

    case "hero2":
      eleHTML = <Hero2 { ...props.options } />;
      break;

    case "hero3":
      eleHTML = <Hero3 { ...props.options } />
      break;
    case "hero4":
      eleHTML = <Hero4 { ...props.options } />
      break;
    case "hero5":
      eleHTML = <Hero5R { ...props.options } windowDimension={ props.windowDimension }/>
      break;

    case "hero6":
      eleHTML = <Hero6R { ...props.options } backgroundLightGrey={ props.backgroundLightGrey }
                        windowDimension={ props.windowDimension }/>
      break;

    default:
      break;
  }

  return eleHTML;
}

function Hero1(props) {
  const eleHTML = (
    <Box
      className={ styles.hero1 }
      sx={ (theme) => ({
        height: 804,
        background: `radial-gradient(50.3% 50.3% at 50% 47.92%, ${ variables.primaryColor }66 0%, ${ variables.secondColor }66 29.69%, rgba(190, 214, 228, 0.2) 61.98%, rgba(255, 255, 255, 0) 100%)`,
        display: "flex",
        justifyContent: "center",
      }) }
    >
      <Group
        style={ {width: 1399} }
        spacing={ 205 }
        align="center"
        position="center"
        noWrap
      >
        <div className={ styles.boxImg }>
          <Image
            radius={ 40 }
            height={ 576 }
            src={ props.image }
            alt="photo hero"
            className={ styles.effectImg }
          />
        </div>
        <Stack style={ {width: 608} }>
          <Text className={ globals.heading1 }>{ props.title }</Text>
          <div className={ styles.boxDivider }>
            <Divider size={ 1 } color={ variables.primaryColor }/>
          </div>
          <Text style={ {lineHeight: "32px"} } lineClamp={ 3 }>
            { props.description }
          </Text>
          <Group
            noWrap
            mt={ 56 }
            spacing={ 28 }
            align="center"
            position="center"
          >
            <Stack spacing={ 8 } className={ styles.stackText }>
              <Text className={ styles.dataNumber }>
                { props.data1.number }
              </Text>
              <Text>{ props.data1.text }</Text>
            </Stack>
            <Divider
              orientation="vertical"
              color={ variables.primaryColor }
              className={ styles.divider }
            />
            <Stack spacing={ 8 } className={ styles.stackText }>
              <Text className={ styles.dataNumber }>
                { props.data2.number }
              </Text>
              <Text>{ props.data2.text }</Text>
            </Stack>
            <Divider
              orientation="vertical"
              color={ variables.primaryColor }
              className={ styles.divider }
            />
            <Stack spacing={ 8 } className={ styles.stackText }>
              <Text className={ styles.dataNumber }>
                { props.data3.number }
              </Text>
              <Text>{ props.data3.text }</Text>
            </Stack>
          </Group>
          {/* <Button
                radius={40}
                mt={34}
                style={{
                  height: "auto",
                  background: variables.primaryColor,
                  fontFamily: "Avenir",
                  fontWeight: 500,
                  fontSize: 22,
                  padding: "19px 0px",
                }}
              >
                Get started
              </Button> */ }
        </Stack>
      </Group>
    </Box>
  );
  return eleHTML;
}

function Hero2(props) {
  const eleHTML = (
    <Box className={ styles.hero2 }>
      <Stack align="center" spacing={ 24 } w={ 750 }>
        <Text className={ styles.title } align="center">
          { props.title }
        </Text>
        <Text className={ styles.description } align="center" lineClamp={ 4 }>
          { props.description }
        </Text>
        <Button
          bg={ variables.primaryColor }
          radius={ 36 }
          fz={ 24 }
          h={ 66 }
          w={ 170 }
        >
          Get Started
        </Button>
      </Stack>
    </Box>
  );
  return eleHTML;
}

function Hero3(props) {
  const eleHTML = (
    <Box className={ styles.hero3 }>
      <Box className={ styles.list }>
        { props.data.map((ele, index) => (
          <Stack key={ `hero3-${ index }` } w={ 300 } px={ 32 }>
            <Text className={ styles.title }>{ ele.title }</Text>
            <Text lineClamp={ 2 }>{ ele.text }</Text>
          </Stack>
        )) }
      </Box>
      <Image
        src={
          props.image ? props.image : "/images/no-img.jpeg"
        }
        radius={ 24 }
        alt="hero 3"
        height={ 500 }
        className={ styles.image }
      />
    </Box>
  );
  return eleHTML;
}

function Hero4(props) {
  const eleHTML = (
    <Box className={ styles.hero4 }>
      <Group position='center' spacing={ 64 } align='center' h={ '100%' } noWrap>
        <Stack spacing={ 24 }>
          <Text lineClamp={ 2 } className={ styles.title }>{ props.title }</Text>
          <Text className={ styles.subtitle }>{ props.subtitle }</Text>
        </Stack>
        <Image width={ 793 } height={ 530 } src={ props.image } alt='' withPlaceholder
               placeholder={ <Image src={ props.image } alt=""/> }/>
      </Group>
    </Box>
  );
  return eleHTML;
}

function Hero5R(props) {
  const divider = (props.windowDimension.width < 1440) ? (1440 / props.windowDimension.width) : 1;
  let eleHTML = <></>;

  const imageBoxCSS = {
    width: 496 / divider,
    height: 552 / divider,
    border: `${ 12 / divider }px solid ${ variables.primaryColor }`,
    borderRadius: 40 / divider,
    marginLeft: 32 / divider,
    position: 'relative'

  };
  const effectImageCSS = {
    left: -32 / divider,
    top: -32 / divider,
    position: 'absolute'
  }

  const dividerCSS = {
    height: 88 / divider,
    position: 'relative',
    top: 11 / divider,
  }


  eleHTML = (
    <BackgroundImage
      src={ props.backgroundImage }
      radius="sm"
      style={ {marginBottom: -44, padding: 64 / divider} }
    >
      <Group spacing={ 100 / divider } style={ {height: 680 / divider} }>
        <div style={ imageBoxCSS }>
          <Image
            radius={ 40 / divider }
            width={ 464 / divider }
            height={ 520 / divider }
            src={ props.image }
            alt="photo hero"
            style={ effectImageCSS }
          />
        </div>
        <Stack style={ {width: 640 / divider, height: "auto"} } spacing={ 60 / divider }>
          <Stack spacing={ 16 / divider }>
            <Text fz={ 48 / divider } fw={ 700 } c={ variables.primaryColor }>{ props.title }</Text>
            <div style={ {width: 100 / divider} }>
              <Divider size={ 1 } color={ variables.secondColor }/>
            </div>
            <Text style={ {lineHeight: 2} } fz={ 18 / divider }>
              { parse(props.description) }
            </Text>
          </Stack>
          <Group noWrap
                 spacing={ 28 / divider }
                 align="center"
                 position="apart">
            <Stack spacing={ 8 / divider } style={ {width: 176 / divider, height: 110 / divider} }>
              <Text fz={ 48 / divider } fw={ 700 } c={ variables.primaryColor }>
                { props.data1.number }
              </Text>
              <Text fz={ 15 / divider }>{ props.data1.text }</Text>
            </Stack>
            <Divider
              orientation="vertical"
              color={ variables.primaryColor }
              style={ dividerCSS }
            />
            <Stack spacing={ 8 / divider } style={ {width: 176 / divider, height: 110 / divider} }>
              <Text fz={ 48 / divider } fw={ 700 } c={ variables.primaryColor }>
                { props.data2.number }
              </Text>
              <Text fz={ 15 / divider }>{ props.data2.text }</Text>
            </Stack>
            <Divider
              orientation="vertical"
              color={ variables.primaryColor }
              style={ dividerCSS }
            />
            <Stack spacing={ 8 / divider } style={ {width: 176 / divider, height: 110 / divider} }>
              <Text fz={ 48 / divider } fw={ 700 } c={ variables.primaryColor }>
                { props.data3.number }
              </Text>
              <Text fz={ 15 / divider }>{ props.data3.text }</Text>
            </Stack>
          </Group>


        </Stack>
      </Group>
    </BackgroundImage>
  );
  return eleHTML;
}

function Hero6R(props) {
  const divider = props.windowDimension.width < 1440 ? 1440 / props.windowDimension.width : 1;

  const buttonCSS = {
    background: variables.primaryColor,
    borderRadius: 36 / divider,
    fontSize: 13 / divider,
    fontWeight: 700,
    height: 52 / divider,
    width: 280 / divider,
    '&:hover': {
      backgroundColor: variables.hoverColor
    }
  }

  const componentText =
    <Stack style={ {width: '50%'} } spacing={ 44 / divider }>
      <Text fz={ 32 / divider } fw={ 700 }>
        { props.title }
      </Text>

      <Text color="dimmed" fz={ 16 / divider }>
        { props.description }
      </Text>

      <Button style={ buttonCSS }>
        Xem tin tuyển dụng
      </Button>

    </Stack>


  const componentPic =
    <>
      <Image width={ 500 / divider }
             height={ 500 / divider }
             radius={ 16 / divider }
             src={ props.image }
             alt="photo hero"
      />
    </>

  const eleHTML = (
    <>
      <Group
        style={ {width: "100%", padding: `${ 44 / divider }px ${ 64 / divider }px`} }
        spacing={ 138 / divider }
        align="center"
        position={ props.isImageLeft ? 'left' : 'apart' }
        background={ props.backgroundLightGrey }
        noWrap>
        { props.isImageLeft ? componentPic : componentText }
        { props.isImageLeft ? componentText : componentPic }
      </Group>
    </>
  );

  return eleHTML;
}
